<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home1');
});
Route::get('/home1', function () {
    return view('home1');
});

Route::get('/home12', function () {
    return view('home2');
})->middleware('auth:web');

Route::get('/register12', function () {
    return view('register');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware('auth:web');

Route::get('/report', function () {
    return view('report');
})->middleware('auth:web');

Route::get('/plant_report', function () {
    return view('plant_report');
})->middleware('auth:web');

Route::get('/weekly_report', function () {
    return view('weekly_report');
})->middleware('auth:web');

Route::get('/user_info', function () {
    return view('user_info');
})->middleware('auth:web');

Route::get('/login', function () {
    return view('auth/login');
});

Auth::routes();
Route::get('/logout12', 'DataController@userlogout')->name('users.logout');
Route::post('/logout12', 'DataController@userlogout')->name('users.logout');
