<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="{{asset('demo/demo.css')}}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
      @extends('sidebar')
    <div class="main-panel">
      <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Dashboard</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <!-- <form class="navbar-form">
              <div class="input-group no-border">
                <input type="text" value="" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
              </div>
            </form> -->
            <ul class="navbar-nav">
              <!-- <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">dashboard</i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li> -->
              <!-- <li class="nav-item dropdown">
                <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">notifications</i>
                  <span class="notification">5</span>
                  <p class="d-lg-none d-md-block">
                    Some Actions
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Mike John responded to your email</a>
                  <a class="dropdown-item" href="#">You have 5 new tasks</a>
                  <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                  <a class="dropdown-item" href="#">Another Notification</a>
                  <a class="dropdown-item" href="#">Another One</a>
                </div>
              </li>-->
              <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="material-icons">person</i> 
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
              </li>
            </ul>
            <form id="logout-form" action="{{route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
            
          <div class="row">
            <div class="offset-4 col-md-4 ">

                <div class="form-group">
                    <label for="exampleSelect1">Select Your Plant Report</label>
                    <select class="form-control" id="exampleSelect1">
                      <option>Plant 1</option>
                      <option>Plant 2</option>
                      <option>plant 3</option>
                      
                    </select>
                  </div>

            </div>
          </div>
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title">First Week Plant Report</h4>
              
            </div>
            <div class="card-body">
              <div id="typography">
                <div class="card-title">
                  <h2>Plants Report</h2>
                </div>
                
                <div class="row">
                  <div class="col-md-4 offset-md-4">
                 <h3 class="text-center">
                 Plant Image
                  </h3>
                  <img class="card-img-top text-center" src="{{asset('img\7e2b0164-6bd8-4e73-9c7a-cb13ca27d4b2_1.bc73beef4998fc7733474e318aeca009.jpeg')}}" alt="Card image cap">

                  </div>
                  <div class="col-md-12">
                      <h3 class="text-center">
                      Plant information "Update"
                       </h3>
                       <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Plant name</label>
                            <input name="plant_name"type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Height</label>
                            <input name="height"type="email" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Width</label>
                            <input name="width"type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Root type</label>
                            <input name="root_type"type="text" class="form-control">
                          </div>
                        </div>
                       </div>
                        <div class="row">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Origin</label>
                            <input name="origin" type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">leaf type</label>
                            <input name="leaf_type"type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">leaf shape</label>
                            <input name="leaf_shape" type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="bmd-label-floating lable-design">Leaf Color</label>
                            <input name="leaf_color" type="text" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-4 offset-md-4">
                          <button class="btn btn-success btn-block">Update Data</button>
                        </div>
                      </div>

                       </div>
                       
                       
                            
                </div>
              </div>
            </div>
          </div>

          <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Second Week Plant Report</h4>
              </div>
              <div class="card-body">
                <div id="typography">
                  <div class="card-title">
                    <h2>Plants Report</h2>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                   <h3 class="text-center">
                   Plant Image
                    </h3>
                    <img class="card-img-top" style="width: 38%; margin-left: 150px;" src="C:\Users\HP\Desktop\material-dashboard-html-v2.1.0\material-dashboard-html-v2.1.0\assets\img\7e2b0164-6bd8-4e73-9c7a-cb13ca27d4b2_1.bc73beef4998fc7733474e318aeca009.jpeg" alt="Card image cap">
  
                    </div>
                    <div class="col-md-6">
                        <h3 class="text-center">
                        Plant info
                         </h3>
                         <ul>
                           <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Height:</li>
                           <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Width:</li>
                           <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Root:</li>
                           <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Leaves:</li>
  
                         </ul>
                         </div>
                         
                              
                  </div>
                </div>
              </div>
            </div>





            <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title">Third Week Plant Report</h4>
                </div>
                <div class="card-body">
                  <div id="typography">
                    <div class="card-title">
                      <h2>Plants Report</h2>
                      
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                     <h3 class="text-center">
                     Plant Image
                      </h3>
                      <img class="card-img-top" style="width: 38%; margin-left: 150px;" src="C:\Users\HP\Desktop\material-dashboard-html-v2.1.0\material-dashboard-html-v2.1.0\assets\img\7e2b0164-6bd8-4e73-9c7a-cb13ca27d4b2_1.bc73beef4998fc7733474e318aeca009.jpeg" alt="Card image cap">
    
                      </div>
                      <div class="col-md-6">
                          <h3 class="text-center">
                          Plant info
                           </h3>
                           <ul>
                             <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Height:</li>
                             <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Width:</li>
                             <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Root:</li>
                             <li style="list-style: none; border-bottom: 1px solid grey; font-size: 20px" class="text-left"> Leaves:</li>
    
                           </ul>
                           </div>
                           
                                
                    </div>
                  </div>
                </div>
              </div>









        </div>
      </div>



      
  <!--   Core JS Files   -->
  <script src="{{asset('js/core/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/core/popper.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/core/bootstrap-material-design.min.js')}}" type="text/javascript"></script>
  <script src="{{asset('js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->
  <script src="{{asset('js/plugins/chartist.min.js')}}"></script>
  <!--  Notifications Plugin    -->
  <script src="{{asset('js/plugins/bootstrap-notify.js')}}"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="{{asset('js/material-dashboard.min.js?v=2.1.0')}}" type="text/javascript"></script>
  <!-- Material Dashboard DEMO methods, don't include it in your project! -->
  <script src="{{asset('demo/demo.js')}}"></script>
</body>

</html>