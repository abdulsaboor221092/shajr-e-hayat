@extends('layouts.app')


@section('content')


<!doctype html>
<html lang="zxx">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
      <div class="content">
          <div style="margin-top: 100px;"class="container-fluid">
            <div class="row">
              <div class="col-md-4 offset-md-4">

                <div class="card">
                  <div class="card-header card-header-primary text-center">
                    <h3 class="card-title">Login your profile </h3>
                    
                   
                  </div>
                  <div class="card-body">
                      <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                

                            </div>
                        </div>
                    </form>
                    <div class="col-md-12 text-center">
                      

                        <a href="{{'register12'}}">
                        <h6 style="color: green"> Create Account</h6>
                      </a>
                      
                        </div>
                  </div>
                </div>
                </div>

              
              </div>
              </div>
            </div>
          <!-- your content here -->
        
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script type="text/javascript" src="{{asset('js/core/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/core/popper.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/core/bootstrap-material-design.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

  <script type="text/javascript" src="{{asset('js/plugins/chartist.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/plugins/bootstrap-notify.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/material-dashboard.min.js?v=2.1.0')}}"></script>




  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->

  <!--  Notifications Plugin    -->

</body>

</html>