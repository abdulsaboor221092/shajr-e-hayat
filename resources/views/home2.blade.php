<!DOCTYPE html>
<html lang="zxx">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PDTWJ3Z');</script>
    <!-- End Google Tag Manager -->
    <title>Xero - Real Estate HTML Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <!-- External CSS libraries -->
     <link rel="stylesheet" type="text/css" href="{{asset('css/app1.css')}}">
       <link rel="stylesheet" type="text/css" href="{{asset('css/skins/default.css')}}">
      <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/layout.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/magnific-popup.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.selectBox.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/rangeslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/leaflet.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/map.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/flaticon/font/flaticon.css')}}">


    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
  

    <!-- Google fonts -->

    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPoppins:400,500,700,800,900%7CRoboto:100,300,400,400i,500,700">

    <!-- Custom Stylesheet -->
    <link  rel="stylesheet" href="{{asset('css/style1.css')}}">
    <link rel="stylesheet" id="style_sheet" href="{{asset('css/skins/default.css')}}">

</head>
<body id="top">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PDTWJ3Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- <div class="page_loader"></div> -->

<!-- main header start -->

<header class="main-header sticky-header" style="background-color: rgba(0, 0, 0, .4);">
    <div class="container">
        <div class="row" style="padding-left: 0px;">
            <div class="col-12" style="padding-left: 0px;">
                <nav class="navbar navbar-expand-lg navbar-light rounded" >
                    
                    <a class="navbar-brand  navbar-brand d-flex w-50 mr-auto" href="home1"  >
                     <img src="{{asset('img/logo.png')}}" style="width: 300px; " >
                    </a>
                    
                      
                    <div class="navbar-collapse collapse w-100" id="navbar">
                        <ul class="navbar-nav w-100 justify-content-center">
            
                           
                            
                       <li class="nav-item dropdown active">
                                <a class="nav-link dropdown-toggle" href="{{'login'}}" >
                                    LOGIN
                                </a>
                           
                            </li>
 <li class="nav-item dropdown active">
                                    <a class="nav-link dropdown-toggle"  style="" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('LOGOUT') }}
                                    </a>
  </li>            
                            
                        </ul>

                        <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
                            
                        </ul>
                          <form id="logout-form" action="{{route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>

<!-- main header end -->

<!-- Banner start -->

<!-- Testimonial end -->


<!-- intro section start -->

<!-- intro section end -->

<!-- Footer start -->


<!-- External JS libraries -->
<script type="text/javascript" src="{{asset('js/jquery-2.2.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.selectBox.js')}}"></script>
<script type="text/javascript" src="{{asset('js/rangeslider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.filterizr.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/backstretch.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.countdown.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.scrollUp.js')}}"></script>
<script type="text/javascript" src="{{asset('js/particles.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/typed.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mb.YTPlayer.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet-providers.js')}}"></script>
<script type="text/javascript" src="{{asset('js/leaflet.markercluster.js')}}"></script>
<script type="text/javascript" src="{{asset('js/maps.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0N5pbJN10Y1oYFRd0MJ_v2g8W2QT74JE"></script>
<script type="text/javascript" src="{{asset('js/ie-emulation-modes-warning.js')}}"></script>
<script type="text/javascript" src="{{asset('js/app1.js')}}"></script>
<script type="text/javascript" src="{{asset('js/app.js')}}"></script>



</body>
</html>
