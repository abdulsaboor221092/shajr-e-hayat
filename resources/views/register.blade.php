
@section('content')

<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/apple-icon.png')}}">
  <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="{{asset('css/material-dashboard.css?v=2.1.0')}}" rel="stylesheet" />
</head>

<body class="">
  <div class="wrapper ">
      <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-4 offset-md-4">
                <div class="card">
                  <div class="card-header card-header-primary text-center">
                    <h3 class="card-title">Register your profile</h3>
                    
                  </div>
                  <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ __('Register') }}">
                        @csrf

                        <div class="row">
                           <div class="col-md-12">
                            <div class="form-group">
                            <label for="name" class="bmd-label-floating">{{ __('Name') }}</label>

                           
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                      </div>

                        <div class="row">
                           <div class="col-md-12">
                            <div class="form-group">
                            <label for="email" class="bmd-label-floating">{{ __('E-Mail Address') }}</label>

                           
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
</div>
                        <div class="row">
                           <div class="col-md-12">
                            <div class="form-group">
                            <label for="password" class="bmd-label-floating">{{ __('Password') }}</label>

                           
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
</div>
      
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                            <label for="password-confirm" class="bmd-label-floating">{{ __('Confirm Password') }}</label>

                            
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                      </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
              </div>
          <!-- your content here -->
        
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script type="text/javascript" src="{{asset('js/core/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/core/popper.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/core/bootstrap-material-design.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/plugins/perfect-scrollbar.jquery.min.js')}}"></script>

  <script type="text/javascript" src="{{asset('js/plugins/chartist.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/plugins/bootstrap-notify.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/material-dashboard.min.js?v=2.1.0')}}"></script>




  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
  <!-- Chartist JS -->

  <!--  Notifications Plugin    -->
</body>

</html>