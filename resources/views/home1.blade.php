<!DOCTYPE html>
<html class="no-js" lang="en-US" itemscope itemtype="https://schema.org/WebPage">

<!-- head -->
<head>

<!-- meta -->
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="description" content="BeEco | Best WordPress theme for eco people" />
<link rel="alternate" hreflang="en-us" href="https://demos.mictronicx.com/shajar-e-hayat/" />

<link rel="shortcut icon" href="https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/images/favicon.ico" />

<!-- wp_head() -->
<title>Splendid Logo &#8211; We Create What You Want</title>
<!-- script | dynamic -->
<script id="mfn-dnmc-config-js">
    
//<![CDATA[
window.mfn_ajax = "https://demos.mictronicx.com/shajar-e-hayat/wp-admin/admin-ajax.php";
window.mfn = {mobile_init:1240,nicescroll:40,parallax:"translate3d",responsive:1,retina_js:0};
window.mfn_lightbox = {disable:false,disableMobile:false,title:false,};
window.mfn_sliders = {blog:0,clients:0,offer:0,portfolio:0,shop:0,slider:0,testimonials:0};
//]]>
</script>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Splendid Logo &raquo; Feed" href="https://demos.mictronicx.com/shajar-e-hayat/feed/" />
<link rel="alternate" type="application/rss+xml" title="Splendid Logo &raquo; Comments Feed" href="https://demos.mictronicx.com/shajar-e-hayat/comments/feed/" />
<link rel="alternate" type="application/rss+xml" title="Splendid Logo &raquo; Home Comments Feed" href="https://demos.mictronicx.com/shajar-e-hayat/home/feed/" />


        <script type="text/javascript">
            window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.4\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/demos.mictronicx.com\/shajar-e-hayat\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.7"}};
            !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56692,8205,9792,65039],[55357,56692,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
        </script>
        <style type="text/css">
img.wp-smiley,
img.emoji {
    display: inline !important;
    border: none !important;
    box-shadow: none !important;
    height: 1em !important;
    width: 1em !important;
    margin: 0 .07em !important;
    vertical-align: -0.1em !important;
    background: none !important;
    padding: 0 !important;
}
</style>
<link rel='stylesheet' id='layerslider-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/LayerSlider/static/layerslider/css/layerslider.css?ver=6.7.1' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='pp-bootstrap-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/ppress/assets/css/flat-ui/bs/css/bs.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='pp-flat-ui-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/ppress/assets/css/flat-ui/css/flat-ui.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='ppcore-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/ppress/assets/css/ppcore.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.7.2' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='theme-my-login-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/theme-my-login/assets/styles/theme-my-login.min.css?ver=7.0.4' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/style.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-base-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/css/base.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-layout-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/css/layout.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-shortcodes-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/css/shortcodes.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-animations-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/assets/animations/animations.min.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-jquery-ui-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/assets/ui/jquery.ui.all.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-jplayer-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/assets/jplayer/css/jplayer.blue.monday.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='mfn-responsive-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/css/responsive.css?ver=20.8.8.2' type='text/css' media='all' />
<link rel='stylesheet' id='Lato-css'  href='https://fonts.googleapis.com/css?family=Lato%3A1%2C300%2C400%2C400italic%2C700%2C700italic&#038;ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='Merriweather-css'  href='https://fonts.googleapis.com/css?family=Merriweather%3A1%2C300%2C400%2C400italic%2C700%2C700italic&#038;ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='fancybox-css'  href='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min.css?ver=1.6.2' type='text/css' media='screen' />
<script type='text/javascript'>
/* <![CDATA[ */
var LS_Meta = {"v":"6.7.1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/LayerSlider/static/layerslider/js/greensock.js?ver=1.19.0'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.kreaturamedia.jquery.js?ver=6.7.1'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/LayerSlider/static/layerslider/js/layerslider.transitions.js?ver=6.7.1'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.7.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.7.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var _zxcvbnSettings = {"src":"https:\/\/demos.mictronicx.com\/shajar-e-hayat\/wp-includes\/js\/zxcvbn.min.js"};
/* ]]> */
</script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/zxcvbn-async.min.js?ver=1.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var pwsL10n = {"unknown":"Password strength unknown","short":"Very weak","bad":"Weak","good":"Medium","strong":"Strong","mismatch":"Mismatch"};
/* ]]> */
</script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-admin/js/password-strength-meter.min.js?ver=4.9.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var themeMyLogin = {"action":"","errors":[]};
/* ]]> */
</script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/theme-my-login/assets/scripts/theme-my-login.min.js?ver=7.0.4'></script>
<meta name="generator" content="Powered by LayerSlider 6.7.1 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." />
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<link rel='https://api.w.org/' href='https://demos.mictronicx.com/shajar-e-hayat/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://demos.mictronicx.com/shajar-e-hayat/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://demos.mictronicx.com/shajar-e-hayat/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.9.7" />
<link rel="canonical" href="https://demos.mictronicx.com/shajar-e-hayat/" />
<link rel='shortlink' href='https://demos.mictronicx.com/shajar-e-hayat/' />
<link rel="alternate" type="application/json+oembed" href="https://demos.mictronicx.com/shajar-e-hayat/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemos.mictronicx.com%2Fshajar-e-hayat%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://demos.mictronicx.com/shajar-e-hayat/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fdemos.mictronicx.com%2Fshajar-e-hayat%2F&#038;format=xml" />
<!-- style | background -->
<style id="mfn-dnmc-bg-css">
body:not(.template-slider) #Header_wrapper{background-image:url(https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/home_eco_subheader.jpg)}
</style>
<!-- style | dynamic -->
<style id="mfn-dnmc-style-css">
@media only screen and (min-width: 1240px){body:not(.header-simple) #Top_bar #menu{display:block!important}.tr-menu #Top_bar #menu{background:none!important}#Top_bar .menu > li > ul.mfn-megamenu{width:984px}#Top_bar .menu > li > ul.mfn-megamenu > li{float:left}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-1{width:100%}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-2{width:50%}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-3{width:33.33%}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-4{width:25%}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-5{width:20%}#Top_bar .menu > li > ul.mfn-megamenu > li.mfn-megamenu-cols-6{width:16.66%}#Top_bar .menu > li > ul.mfn-megamenu > li > ul{display:block!important;position:inherit;left:auto;top:auto;border-width:0 1px 0 0}#Top_bar .menu > li > ul.mfn-megamenu > li:last-child > ul{border:0}#Top_bar .menu > li > ul.mfn-megamenu > li > ul li{width:auto}#Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title{text-transform:uppercase;font-weight:400;background:none}#Top_bar .menu > li > ul.mfn-megamenu a .menu-arrow{display:none}.menuo-right #Top_bar .menu > li > ul.mfn-megamenu{left:auto;right:0}.menuo-right #Top_bar .menu > li > ul.mfn-megamenu-bg{box-sizing:border-box}#Top_bar .menu > li > ul.mfn-megamenu-bg{padding:20px 166px 20px 20px;background-repeat:no-repeat;background-position:right bottom}.rtl #Top_bar .menu > li > ul.mfn-megamenu-bg{padding-left:166px;padding-right:20px;background-position:left bottom}#Top_bar .menu > li > ul.mfn-megamenu-bg > li{background:none}#Top_bar .menu > li > ul.mfn-megamenu-bg > li a{border:none}#Top_bar .menu > li > ul.mfn-megamenu-bg > li > ul{background:none!important;-webkit-box-shadow:0 0 0 0;-moz-box-shadow:0 0 0 0;box-shadow:0 0 0 0}.mm-vertical #Top_bar .container{position:relative;}.mm-vertical #Top_bar .top_bar_left{position:static;}.mm-vertical #Top_bar .menu > li ul{box-shadow:0 0 0 0 transparent!important;background-image:none;}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu{width:98%!important;margin:0 1%;padding:20px 0;}.mm-vertical.header-plain #Top_bar .menu > li > ul.mfn-megamenu{width:100%!important;margin:0;}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li{display:table-cell;float:none!important;width:10%;padding:0 15px;border-right:1px solid rgba(0, 0, 0, 0.05);}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li:last-child{border-right-width:0}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li.hide-border{border-right-width:0}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li a{border-bottom-width:0;padding:9px 15px;line-height:120%;}.mm-vertical #Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title{font-weight:700;}.rtl .mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li:first-child{border-right-width:0}.rtl .mm-vertical #Top_bar .menu > li > ul.mfn-megamenu > li:last-child{border-right-width:1px}#Header_creative #Top_bar .menu > li > ul.mfn-megamenu{width:980px!important;margin:0;}.header-plain:not(.menuo-right) #Header .top_bar_left{width:auto!important}.header-stack.header-center #Top_bar #menu{display:inline-block!important}.header-simple #Top_bar #menu{display:none;height:auto;width:300px;bottom:auto;top:100%;right:1px;position:absolute;margin:0}.header-simple #Header a.responsive-menu-toggle{display:block;right:10px}.header-simple #Top_bar #menu > ul{width:100%;float:left}.header-simple #Top_bar #menu ul li{width:100%;padding-bottom:0;border-right:0;position:relative}.header-simple #Top_bar #menu ul li a{padding:0 20px;margin:0;display:block;height:auto;line-height:normal;border:none}.header-simple #Top_bar #menu ul li a:after{display:none}.header-simple #Top_bar #menu ul li a span{border:none;line-height:44px;display:inline;padding:0}.header-simple #Top_bar #menu ul li.submenu .menu-toggle{display:block;position:absolute;right:0;top:0;width:44px;height:44px;line-height:44px;font-size:30px;font-weight:300;text-align:center;cursor:pointer;color:#444;opacity:0.33;}.header-simple #Top_bar #menu ul li.submenu .menu-toggle:after{content:"+"}.header-simple #Top_bar #menu ul li.hover > .menu-toggle:after{content:"-"}.header-simple #Top_bar #menu ul li.hover a{border-bottom:0}.header-simple #Top_bar #menu ul.mfn-megamenu li .menu-toggle{display:none}.header-simple #Top_bar #menu ul li ul{position:relative!important;left:0!important;top:0;padding:0;margin:0!important;width:auto!important;background-image:none}.header-simple #Top_bar #menu ul li ul li{width:100%!important;display:block;padding:0;}.header-simple #Top_bar #menu ul li ul li a{padding:0 20px 0 30px}.header-simple #Top_bar #menu ul li ul li a .menu-arrow{display:none}.header-simple #Top_bar #menu ul li ul li a span{padding:0}.header-simple #Top_bar #menu ul li ul li a span:after{display:none!important}.header-simple #Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title{text-transform:uppercase;font-weight:400}.header-simple #Top_bar .menu > li > ul.mfn-megamenu > li > ul{display:block!important;position:inherit;left:auto;top:auto}.header-simple #Top_bar #menu ul li ul li ul{border-left:0!important;padding:0;top:0}.header-simple #Top_bar #menu ul li ul li ul li a{padding:0 20px 0 40px}.rtl.header-simple #Top_bar #menu{left:1px;right:auto}.rtl.header-simple #Top_bar a.responsive-menu-toggle{left:10px;right:auto}.rtl.header-simple #Top_bar #menu ul li.submenu .menu-toggle{left:0;right:auto}.rtl.header-simple #Top_bar #menu ul li ul{left:auto!important;right:0!important}.rtl.header-simple #Top_bar #menu ul li ul li a{padding:0 30px 0 20px}.rtl.header-simple #Top_bar #menu ul li ul li ul li a{padding:0 40px 0 20px}.menu-highlight #Top_bar .menu > li{margin:0 2px}.menu-highlight:not(.header-creative) #Top_bar .menu > li > a{margin:20px 0;padding:0;-webkit-border-radius:5px;border-radius:5px}.menu-highlight #Top_bar .menu > li > a:after{display:none}.menu-highlight #Top_bar .menu > li > a span:not(.description){line-height:50px}.menu-highlight #Top_bar .menu > li > a span.description{display:none}.menu-highlight.header-stack #Top_bar .menu > li > a{margin:10px 0!important}.menu-highlight.header-stack #Top_bar .menu > li > a span:not(.description){line-height:40px}.menu-highlight.header-transparent #Top_bar .menu > li > a{margin:5px 0}.menu-highlight.header-simple #Top_bar #menu ul li,.menu-highlight.header-creative #Top_bar #menu ul li{margin:0}.menu-highlight.header-simple #Top_bar #menu ul li > a,.menu-highlight.header-creative #Top_bar #menu ul li > a{-webkit-border-radius:0;border-radius:0}.menu-highlight:not(.header-fixed):not(.header-simple) #Top_bar.is-sticky .menu > li > a{margin:10px 0!important;padding:5px 0!important}.menu-highlight:not(.header-fixed):not(.header-simple) #Top_bar.is-sticky .menu > li > a span{line-height:30px!important}.header-modern.menu-highlight.menuo-right .menu_wrapper{margin-right:20px}.menu-line-below #Top_bar .menu > li > a:after{top:auto;bottom:-4px}.menu-line-below #Top_bar.is-sticky .menu > li > a:after{top:auto;bottom:-4px}.menu-line-below-80 #Top_bar:not(.is-sticky) .menu > li > a:after{height:4px;left:10%;top:50%;margin-top:20px;width:80%}.menu-line-below-80-1 #Top_bar:not(.is-sticky) .menu > li > a:after{height:1px;left:10%;top:50%;margin-top:20px;width:80%}.menu-link-color #Top_bar .menu > li > a:after{display:none!important}.menu-arrow-top #Top_bar .menu > li > a:after{background:none repeat scroll 0 0 rgba(0,0,0,0)!important;border-color:#ccc transparent transparent;border-style:solid;border-width:7px 7px 0;display:block;height:0;left:50%;margin-left:-7px;top:0!important;width:0}.menu-arrow-top.header-transparent #Top_bar .menu > li > a:after,.menu-arrow-top.header-plain #Top_bar .menu > li > a:after{display:none}.menu-arrow-top #Top_bar.is-sticky .menu > li > a:after{top:0!important}.menu-arrow-bottom #Top_bar .menu > li > a:after{background:none!important;border-color:transparent transparent #ccc;border-style:solid;border-width:0 7px 7px;display:block;height:0;left:50%;margin-left:-7px;top:auto;bottom:0;width:0}.menu-arrow-bottom.header-transparent #Top_bar .menu > li > a:after,.menu-arrow-bottom.header-plain #Top_bar .menu > li > a:after{display:none}.menu-arrow-bottom #Top_bar.is-sticky .menu > li > a:after{top:auto;bottom:0}.menuo-no-borders #Top_bar .menu > li > a span:not(.description){border-right-width:0}.menuo-no-borders #Header_creative #Top_bar .menu > li > a span{border-bottom-width:0}.menuo-right #Top_bar .menu_wrapper{float:right}.menuo-right.header-stack:not(.header-center) #Top_bar .menu_wrapper{margin-right:150px}body.header-creative{padding-left:50px}body.header-creative.header-open{padding-left:250px}body.error404,body.under-construction,body.template-blank{padding-left:0!important}.header-creative.footer-fixed #Footer,.header-creative.footer-sliding #Footer,.header-creative.footer-stick #Footer.is-sticky{box-sizing:border-box;padding-left:50px;}.header-open.footer-fixed #Footer,.header-open.footer-sliding #Footer,.header-creative.footer-stick #Footer.is-sticky{padding-left:250px;}.header-rtl.header-creative.footer-fixed #Footer,.header-rtl.header-creative.footer-sliding #Footer,.header-rtl.header-creative.footer-stick #Footer.is-sticky{padding-left:0;padding-right:50px;}.header-rtl.header-open.footer-fixed #Footer,.header-rtl.header-open.footer-sliding #Footer,.header-rtl.header-creative.footer-stick #Footer.is-sticky{padding-right:250px;}#Header_creative{background:#fff;position:fixed;width:250px;height:100%;left:-200px;top:0;z-index:9002;-webkit-box-shadow:2px 0 4px 2px rgba(0,0,0,.15);box-shadow:2px 0 4px 2px rgba(0,0,0,.15)}#Header_creative .container{width:100%}#Header_creative .creative-wrapper{opacity:0;margin-right:50px}#Header_creative a.creative-menu-toggle{display:block;width:34px;height:34px;line-height:34px;font-size:22px;text-align:center;position:absolute;top:10px;right:8px;border-radius:3px}.admin-bar #Header_creative a.creative-menu-toggle{top:42px}#Header_creative #Top_bar{position:static;width:100%}#Header_creative #Top_bar .top_bar_left{width:100%!important;float:none}#Header_creative #Top_bar .top_bar_right{width:100%!important;float:none;height:auto;margin-bottom:35px;text-align:center;padding:0 20px;top:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#Header_creative #Top_bar .top_bar_right:before{display:none}#Header_creative #Top_bar .top_bar_right_wrapper{top:0}#Header_creative #Top_bar .logo{float:none;text-align:center;margin:15px 0}#Header_creative #Top_bar .menu_wrapper{float:none;margin:0 0 30px}#Header_creative #Top_bar .menu > li{width:100%;float:none;position:relative}#Header_creative #Top_bar .menu > li > a{padding:0;text-align:center}#Header_creative #Top_bar .menu > li > a:after{display:none}#Header_creative #Top_bar .menu > li > a span{border-right:0;border-bottom-width:1px;line-height:38px}#Header_creative #Top_bar .menu li ul{left:100%;right:auto;top:0;box-shadow:2px 2px 2px 0 rgba(0,0,0,0.03);-webkit-box-shadow:2px 2px 2px 0 rgba(0,0,0,0.03)}#Header_creative #Top_bar .menu > li > ul.mfn-megamenu{width:700px!important;}#Header_creative #Top_bar .menu > li > ul.mfn-megamenu > li > ul{left:0}#Header_creative #Top_bar .menu li ul li a{padding-top:9px;padding-bottom:8px}#Header_creative #Top_bar .menu li ul li ul{top:0}#Header_creative #Top_bar .menu > li > a span.description{display:block;font-size:13px;line-height:28px!important;clear:both}#Header_creative #Top_bar .search_wrapper{left:100%;top:auto;bottom:0}#Header_creative #Top_bar a#header_cart{display:inline-block;float:none;top:3px}#Header_creative #Top_bar a#search_button{display:inline-block;float:none;top:3px}#Header_creative #Top_bar .wpml-languages{display:inline-block;float:none;top:0}#Header_creative #Top_bar .wpml-languages.enabled:hover a.active{padding-bottom:9px}#Header_creative #Top_bar a.button.action_button{display:inline-block;float:none;top:16px;margin:0}#Header_creative #Top_bar .banner_wrapper{display:block;text-align:center}#Header_creative #Top_bar .banner_wrapper img{max-width:100%;height:auto;display:inline-block}#Header_creative #Action_bar{display:none;position:absolute;bottom:0;top:auto;clear:both;padding:0 20px;box-sizing:border-box}#Header_creative #Action_bar .social{float:none;text-align:center;padding:5px 0 15px}#Header_creative #Action_bar .social li{margin-bottom:2px}#Header_creative .social li a{color:rgba(0,0,0,.5)}#Header_creative .social li a:hover{color:#000}#Header_creative .creative-social{position:absolute;bottom:10px;right:0;width:50px}#Header_creative .creative-social li{display:block;float:none;width:100%;text-align:center;margin-bottom:5px}.header-creative .fixed-nav.fixed-nav-prev{margin-left:50px}.header-creative.header-open .fixed-nav.fixed-nav-prev{margin-left:250px}.menuo-last #Header_creative #Top_bar .menu li.last ul{top:auto;bottom:0}.header-open #Header_creative{left:0}.header-open #Header_creative .creative-wrapper{opacity:1;margin:0!important;}.header-open #Header_creative .creative-menu-toggle,.header-open #Header_creative .creative-social{display:none}.header-open #Header_creative #Action_bar{display:block}body.header-rtl.header-creative{padding-left:0;padding-right:50px}.header-rtl #Header_creative{left:auto;right:-200px}.header-rtl.nice-scroll #Header_creative{margin-right:10px}.header-rtl #Header_creative .creative-wrapper{margin-left:50px;margin-right:0}.header-rtl #Header_creative a.creative-menu-toggle{left:8px;right:auto}.header-rtl #Header_creative .creative-social{left:0;right:auto}.header-rtl #Footer #back_to_top.sticky{right:125px}.header-rtl #popup_contact{right:70px}.header-rtl #Header_creative #Top_bar .menu li ul{left:auto;right:100%}.header-rtl #Header_creative #Top_bar .search_wrapper{left:auto;right:100%;}.header-rtl .fixed-nav.fixed-nav-prev{margin-left:0!important}.header-rtl .fixed-nav.fixed-nav-next{margin-right:50px}body.header-rtl.header-creative.header-open{padding-left:0;padding-right:250px!important}.header-rtl.header-open #Header_creative{left:auto;right:0}.header-rtl.header-open #Footer #back_to_top.sticky{right:325px}.header-rtl.header-open #popup_contact{right:270px}.header-rtl.header-open .fixed-nav.fixed-nav-next{margin-right:250px}#Header_creative.active{left:-1px;}.header-rtl #Header_creative.active{left:auto;right:-1px;}#Header_creative.active .creative-wrapper{opacity:1;margin:0}.header-creative .vc_row[data-vc-full-width]{padding-left:50px}.header-creative.header-open .vc_row[data-vc-full-width]{padding-left:250px}.header-open .vc_parallax .vc_parallax-inner { left:auto; width: calc(100% - 250px); }.header-open.header-rtl .vc_parallax .vc_parallax-inner { left:0; right:auto; }#Header_creative.scroll{height:100%;overflow-y:auto}#Header_creative.scroll:not(.dropdown) .menu li ul{display:none!important}#Header_creative.scroll #Action_bar{position:static}#Header_creative.dropdown{outline:none}#Header_creative.dropdown #Top_bar .menu_wrapper{float:left}#Header_creative.dropdown #Top_bar #menu ul li{position:relative;float:left}#Header_creative.dropdown #Top_bar #menu ul li a:after{display:none}#Header_creative.dropdown #Top_bar #menu ul li a span{line-height:38px;padding:0}#Header_creative.dropdown #Top_bar #menu ul li.submenu .menu-toggle{display:block;position:absolute;right:0;top:0;width:38px;height:38px;line-height:38px;font-size:26px;font-weight:300;text-align:center;cursor:pointer;color:#444;opacity:0.33;}#Header_creative.dropdown #Top_bar #menu ul li.submenu .menu-toggle:after{content:"+"}#Header_creative.dropdown #Top_bar #menu ul li.hover > .menu-toggle:after{content:"-"}#Header_creative.dropdown #Top_bar #menu ul li.hover a{border-bottom:0}#Header_creative.dropdown #Top_bar #menu ul.mfn-megamenu li .menu-toggle{display:none}#Header_creative.dropdown #Top_bar #menu ul li ul{position:relative!important;left:0!important;top:0;padding:0;margin-left:0!important;width:auto!important;background-image:none}#Header_creative.dropdown #Top_bar #menu ul li ul li{width:100%!important}#Header_creative.dropdown #Top_bar #menu ul li ul li a{padding:0 10px;text-align:center}#Header_creative.dropdown #Top_bar #menu ul li ul li a .menu-arrow{display:none}#Header_creative.dropdown #Top_bar #menu ul li ul li a span{padding:0}#Header_creative.dropdown #Top_bar #menu ul li ul li a span:after{display:none!important}#Header_creative.dropdown #Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title{text-transform:uppercase;font-weight:400}#Header_creative.dropdown #Top_bar .menu > li > ul.mfn-megamenu > li > ul{display:block!important;position:inherit;left:auto;top:auto}#Header_creative.dropdown #Top_bar #menu ul li ul li ul{border-left:0!important;padding:0;top:0}#Header_creative{transition: left .5s ease-in-out, right .5s ease-in-out;}#Header_creative .creative-wrapper{transition: opacity .5s ease-in-out, margin 0s ease-in-out .5s;}#Header_creative.active .creative-wrapper{transition: opacity .5s ease-in-out, margin 0s ease-in-out;}}@media only screen and (min-width: 9999px){#Top_bar.is-sticky{position:fixed!important;width:100%;left:0;top:-60px;height:60px;z-index:701;background:#fff;opacity:.97;filter:alpha(opacity = 97);-webkit-box-shadow:0 2px 5px 0 rgba(0,0,0,0.1);-moz-box-shadow:0 2px 5px 0 rgba(0,0,0,0.1);box-shadow:0 2px 5px 0 rgba(0,0,0,0.1)}.layout-boxed.header-boxed #Top_bar.is-sticky{max-width:1240px;left:50%;-webkit-transform:translateX(-50%);transform:translateX(-50%)}.layout-boxed.header-boxed.nice-scroll #Top_bar.is-sticky{margin-left:-5px}#Top_bar.is-sticky .top_bar_left,#Top_bar.is-sticky .top_bar_right,#Top_bar.is-sticky .top_bar_right:before{background:none}#Top_bar.is-sticky .top_bar_right{top:-4px;height:auto;}#Top_bar.is-sticky .top_bar_right_wrapper{top:15px}.header-plain #Top_bar.is-sticky .top_bar_right_wrapper{top:0}#Top_bar.is-sticky .logo{width:auto;margin:0 30px 0 20px;padding:0}#Top_bar.is-sticky #logo{padding:5px 0!important;height:50px!important;line-height:50px!important}.logo-no-sticky-padding #Top_bar.is-sticky #logo{height:60px!important;line-height:60px!important}#Top_bar.is-sticky #logo img.logo-main{display:none}#Top_bar.is-sticky #logo img.logo-sticky{display:inline;max-height:35px;}#Top_bar.is-sticky .menu_wrapper{clear:none}#Top_bar.is-sticky .menu_wrapper .menu > li > a{padding:15px 0}#Top_bar.is-sticky .menu > li > a,#Top_bar.is-sticky .menu > li > a span{line-height:30px}#Top_bar.is-sticky .menu > li > a:after{top:auto;bottom:-4px}#Top_bar.is-sticky .menu > li > a span.description{display:none}#Top_bar.is-sticky .secondary_menu_wrapper,#Top_bar.is-sticky .banner_wrapper{display:none}.header-overlay #Top_bar.is-sticky{display:none}.sticky-dark #Top_bar.is-sticky{background:rgba(0,0,0,.8)}.sticky-dark #Top_bar.is-sticky #menu{background:rgba(0,0,0,.8)}.sticky-dark #Top_bar.is-sticky .menu > li > a{color:#fff}.sticky-dark #Top_bar.is-sticky .top_bar_right a{color:rgba(255,255,255,.5)}.sticky-dark #Top_bar.is-sticky .wpml-languages a.active,.sticky-dark #Top_bar.is-sticky .wpml-languages ul.wpml-lang-dropdown{background:rgba(0,0,0,0.3);border-color:rgba(0,0,0,0.1)}}@media only screen and (max-width: 1239px){#Top_bar #menu{display:none;height:auto;width:300px;bottom:auto;top:100%;right:1px;position:absolute;margin:0}#Top_bar a.responsive-menu-toggle{display:block}#Top_bar #menu > ul{width:100%;float:left}#Top_bar #menu ul li{width:100%;padding-bottom:0;border-right:0;position:relative}#Top_bar #menu ul li a{padding:0 25px;margin:0;display:block;height:auto;line-height:normal;border:none}#Top_bar #menu ul li a:after{display:none}#Top_bar #menu ul li a span{border:none;line-height:44px;display:inline;padding:0}#Top_bar #menu ul li a span.description{margin:0 0 0 5px}#Top_bar #menu ul li.submenu .menu-toggle{display:block;position:absolute;right:15px;top:0;width:44px;height:44px;line-height:44px;font-size:30px;font-weight:300;text-align:center;cursor:pointer;color:#444;opacity:0.33;}#Top_bar #menu ul li.submenu .menu-toggle:after{content:"+"}#Top_bar #menu ul li.hover > .menu-toggle:after{content:"-"}#Top_bar #menu ul li.hover a{border-bottom:0}#Top_bar #menu ul li a span:after{display:none!important}#Top_bar #menu ul.mfn-megamenu li .menu-toggle{display:none}#Top_bar #menu ul li ul{position:relative!important;left:0!important;top:0;padding:0;margin-left:0!important;width:auto!important;background-image:none!important;box-shadow:0 0 0 0 transparent!important;-webkit-box-shadow:0 0 0 0 transparent!important}#Top_bar #menu ul li ul li{width:100%!important}#Top_bar #menu ul li ul li a{padding:0 20px 0 35px}#Top_bar #menu ul li ul li a .menu-arrow{display:none}#Top_bar #menu ul li ul li a span{padding:0}#Top_bar #menu ul li ul li a span:after{display:none!important}#Top_bar .menu > li > ul.mfn-megamenu a.mfn-megamenu-title{text-transform:uppercase;font-weight:400}#Top_bar .menu > li > ul.mfn-megamenu > li > ul{display:block!important;position:inherit;left:auto;top:auto}#Top_bar #menu ul li ul li ul{border-left:0!important;padding:0;top:0}#Top_bar #menu ul li ul li ul li a{padding:0 20px 0 45px}.rtl #Top_bar #menu{left:1px;right:auto}.rtl #Top_bar a.responsive-menu-toggle{left:20px;right:auto}.rtl #Top_bar #menu ul li.submenu .menu-toggle{left:15px;right:auto;border-left:none;border-right:1px solid #eee}.rtl #Top_bar #menu ul li ul{left:auto!important;right:0!important}.rtl #Top_bar #menu ul li ul li a{padding:0 30px 0 20px}.rtl #Top_bar #menu ul li ul li ul li a{padding:0 40px 0 20px}.header-stack .menu_wrapper a.responsive-menu-toggle{position:static!important;margin:11px 0!important}.header-stack .menu_wrapper #menu{left:0;right:auto}.rtl.header-stack #Top_bar #menu{left:auto;right:0}.admin-bar #Header_creative{top:32px}.header-creative.layout-boxed{padding-top:85px}.header-creative.layout-full-width #Wrapper{padding-top:60px}#Header_creative{position:fixed;width:100%;left:0!important;top:0;z-index:1001}#Header_creative .creative-wrapper{display:block!important;opacity:1!important}#Header_creative .creative-menu-toggle,#Header_creative .creative-social{display:none!important;opacity:1!important;filter:alpha(opacity=100)!important}#Header_creative #Top_bar{position:static;width:100%}#Header_creative #Top_bar #logo{height:50px;line-height:50px;padding:5px 0}#Header_creative #Top_bar #logo img.logo-sticky{max-height:40px!important}#Header_creative #logo img.logo-main{display:none}#Header_creative #logo img.logo-sticky{display:inline-block}.logo-no-sticky-padding #Header_creative #Top_bar #logo{height:60px;line-height:60px;padding:0}.logo-no-sticky-padding #Header_creative #Top_bar #logo img.logo-sticky{max-height:60px!important}#Header_creative #Top_bar #header_cart{top:21px}#Header_creative #Top_bar #search_button{top:20px}#Header_creative #Top_bar .wpml-languages{top:11px}#Header_creative #Top_bar .action_button{top:9px}#Header_creative #Top_bar .top_bar_right{height:60px;top:0}#Header_creative #Top_bar .top_bar_right:before{display:none}#Header_creative #Top_bar .top_bar_right_wrapper{top:0}#Header_creative #Action_bar{display:none}#Header_creative.scroll{overflow:visible!important}}#Header_wrapper, #Intro {background-color: #052606;}#Subheader {background-color: rgba(247, 247, 247, 0);}.header-classic #Action_bar, .header-fixed #Action_bar, .header-plain #Action_bar, .header-split #Action_bar, .header-stack #Action_bar {background-color: #2C2C2C;}#Sliding-top {background-color: #0d6526;}#Sliding-top a.sliding-top-control {border-right-color: #0d6526;}#Sliding-top.st-center a.sliding-top-control,#Sliding-top.st-left a.sliding-top-control {border-top-color: #0d6526;}#Footer {background-color: #0d6526;}body, ul.timeline_items, .icon_box a .desc, .icon_box a:hover .desc, .feature_list ul li a, .list_item a, .list_item a:hover,.widget_recent_entries ul li a, .flat_box a, .flat_box a:hover, .story_box .desc, .content_slider.carouselul li a .title,.content_slider.flat.description ul li .desc, .content_slider.flat.description ul li a .desc, .post-nav.minimal a i {color: #486350;}.post-nav.minimal a svg {fill: #486350;}.themecolor, .opening_hours .opening_hours_wrapper li span, .fancy_heading_icon .icon_top,.fancy_heading_arrows .icon-right-dir, .fancy_heading_arrows .icon-left-dir, .fancy_heading_line .title,.button-love a.mfn-love, .format-link .post-title .icon-link, .pager-single > span, .pager-single a:hover,.widget_meta ul, .widget_pages ul, .widget_rss ul, .widget_mfn_recent_comments ul li:after, .widget_archive ul,.widget_recent_comments ul li:after, .widget_nav_menu ul, .woocommerce ul.products li.product .price, .shop_slider .shop_slider_ul li .item_wrapper .price,.woocommerce-page ul.products li.product .price, .widget_price_filter .price_label .from, .widget_price_filter .price_label .to,.woocommerce ul.product_list_widget li .quantity .amount, .woocommerce .product div.entry-summary .price, .woocommerce .star-rating span,#Error_404 .error_pic i, .style-simple #Filters .filters_wrapper ul li a:hover, .style-simple #Filters .filters_wrapper ul li.current-cat a,.style-simple .quick_fact .title {color: #58b32b;}.themebg,#comments .commentlist > li .reply a.comment-reply-link,#Filters .filters_wrapper ul li a:hover,#Filters .filters_wrapper ul li.current-cat a,.fixed-nav .arrow,.offer_thumb .slider_pagination a:before,.offer_thumb .slider_pagination a.selected:after,.pager .pages a:hover,.pager .pages a.active,.pager .pages span.page-numbers.current,.pager-single span:after,.portfolio_group.exposure .portfolio-item .desc-inner .line,.Recent_posts ul li .desc:after,.Recent_posts ul li .photo .c,.slider_pagination a.selected,.slider_pagination .slick-active a,.slider_pagination a.selected:after,.slider_pagination .slick-active a:after,.testimonials_slider .slider_images,.testimonials_slider .slider_images a:after,.testimonials_slider .slider_images:before,#Top_bar a#header_cart span,.widget_categories ul,.widget_mfn_menu ul li a:hover,.widget_mfn_menu ul li.current-menu-item:not(.current-menu-ancestor) > a,.widget_mfn_menu ul li.current_page_item:not(.current_page_ancestor) > a,.widget_product_categories ul,.widget_recent_entries ul li:after,.woocommerce-account table.my_account_orders .order-number a,.woocommerce-MyAccount-navigation ul li.is-active a,.style-simple .accordion .question:after,.style-simple .faq .question:after,.style-simple .icon_box .desc_wrapper .title:before,.style-simple #Filters .filters_wrapper ul li a:after,.style-simple .article_box .desc_wrapper p:after,.style-simple .sliding_box .desc_wrapper:after,.style-simple .trailer_box:hover .desc,.tp-bullets.simplebullets.round .bullet.selected,.tp-bullets.simplebullets.round .bullet.selected:after,.tparrows.default,.tp-bullets.tp-thumbs .bullet.selected:after{background-color: #58b32b;}.Latest_news ul li .photo, .Recent_posts.blog_news ul li .photo, .style-simple .opening_hours .opening_hours_wrapper li label,.style-simple .timeline_items li:hover h3, .style-simple .timeline_items li:nth-child(even):hover h3,.style-simple .timeline_items li:hover .desc, .style-simple .timeline_items li:nth-child(even):hover,.style-simple .offer_thumb .slider_pagination a.selected {border-color: #58b32b;}a {color: #58b32b;}a:hover {color: #3e9414;}*::-moz-selection {background-color: #58b32b;}*::selection {background-color: #58b32b;}.blockquote p.author span, .counter .desc_wrapper .title, .article_box .desc_wrapper p, .team .desc_wrapper p.subtitle,.pricing-box .plan-header p.subtitle, .pricing-box .plan-header .price sup.period, .chart_box p, .fancy_heading .inside,.fancy_heading_line .slogan, .post-meta, .post-meta a, .post-footer, .post-footer a span.label, .pager .pages a, .button-love a .label,.pager-single a, #comments .commentlist > li .comment-author .says, .fixed-nav .desc .date, .filters_buttons li.label, .Recent_posts ul li a .desc .date,.widget_recent_entries ul li .post-date, .tp_recent_tweets .twitter_time, .widget_price_filter .price_label, .shop-filters .woocommerce-result-count,.woocommerce ul.product_list_widget li .quantity, .widget_shopping_cart ul.product_list_widget li dl, .product_meta .posted_in,.woocommerce .shop_table .product-name .variation > dd, .shipping-calculator-button:after,.shop_slider .shop_slider_ul li .item_wrapper .price del,.testimonials_slider .testimonials_slider_ul li .author span, .testimonials_slider .testimonials_slider_ul li .author span a, .Latest_news ul li .desc_footer,.share-simple-wrapper .icons a {color: #a8a8a8;}h1, h1 a, h1 a:hover, .text-logo #logo { color: #3b4b50; }h2, h2 a, h2 a:hover { color: #409843; }h3, h3 a, h3 a:hover { color: #3b4b50; }h4, h4 a, h4 a:hover, .style-simple .sliding_box .desc_wrapper h4 { color: #3b4b50; }h5, h5 a, h5 a:hover { color: #3b4b50; }h6, h6 a, h6 a:hover,a.content_link .title { color: #3b4b50; }.dropcap, .highlight:not(.highlight_image) {background-color: #58b32b;}a.button, a.tp-button {background-color: #0d6526;color: #ffffff;}.button-stroke a.button, .button-stroke a.button.action_button, .button-stroke a.button .button_icon i, .button-stroke a.tp-button {border-color: #0d6526;color: #ffffff;}.button-stroke a:hover.button, .button-stroke a:hover.tp-button {background-color: #0d6526 !important;color: #fff;}a.button_theme, a.tp-button.button_theme,button, input[type="submit"], input[type="reset"], input[type="button"] {background-color: #58b32b;color: #ffffff;}.button-stroke a.button.button_theme:not(.action_button),.button-stroke a.button.button_theme .button_icon i, .button-stroke a.tp-button.button_theme,.button-stroke button, .button-stroke input[type="submit"], .button-stroke input[type="reset"], .button-stroke input[type="button"] {border-color: #58b32b;color: #58b32b !important;}.button-stroke a.button.button_theme:hover, .button-stroke a.tp-button.button_theme:hover,.button-stroke button:hover, .button-stroke input[type="submit"]:hover, .button-stroke input[type="reset"]:hover, .button-stroke input[type="button"]:hover {background-color: #58b32b !important;color: #ffffff !important;}a.mfn-link {color: #656B6F;}a.mfn-link-2 span, a:hover.mfn-link-2 span:before, a.hover.mfn-link-2 span:before, a.mfn-link-5 span, a.mfn-link-8:after, a.mfn-link-8:before {background: #58b32b;}a:hover.mfn-link {color: #58b32b;}a.mfn-link-2 span:before, a:hover.mfn-link-4:before, a:hover.mfn-link-4:after, a.hover.mfn-link-4:before, a.hover.mfn-link-4:after, a.mfn-link-5:before, a.mfn-link-7:after, a.mfn-link-7:before {background: #3e9414;}a.mfn-link-6:before {border-bottom-color: #3e9414;}.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,.woocommerce input.button,.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover{background-color: #58b32b;color: #fff;}.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt,.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover{background-color: #58b32b;color: #fff;}.woocommerce #respond input#submit.disabled,.woocommerce #respond input#submit:disabled,.woocommerce #respond input#submit[disabled]:disabled,.woocommerce a.button.disabled,.woocommerce a.button:disabled,.woocommerce a.button[disabled]:disabled,.woocommerce button.button.disabled,.woocommerce button.button:disabled,.woocommerce button.button[disabled]:disabled,.woocommerce input.button.disabled,.woocommerce input.button:disabled,.woocommerce input.button[disabled]:disabled{background-color: #58b32b;color: #fff;}.woocommerce #respond input#submit.disabled:hover,.woocommerce #respond input#submit:disabled:hover,.woocommerce #respond input#submit[disabled]:disabled:hover,.woocommerce a.button.disabled:hover,.woocommerce a.button:disabled:hover,.woocommerce a.button[disabled]:disabled:hover,.woocommerce button.button.disabled:hover,.woocommerce button.button:disabled:hover,.woocommerce button.button[disabled]:disabled:hover,.woocommerce input.button.disabled:hover,.woocommerce input.button:disabled:hover,.woocommerce input.button[disabled]:disabled:hover{background-color: #58b32b;color: #fff;}.button-stroke.woocommerce-page #respond input#submit,.button-stroke.woocommerce-page a.button:not(.action_button),.button-stroke.woocommerce-page button.button,.button-stroke.woocommerce-page input.button{border: 2px solid #58b32b !important;color: #58b32b !important;}.button-stroke.woocommerce-page #respond input#submit:hover,.button-stroke.woocommerce-page a.button:not(.action_button):hover,.button-stroke.woocommerce-page button.button:hover,.button-stroke.woocommerce-page input.button:hover{background-color: #58b32b !important;color: #fff !important;}.column_column ul, .column_column ol, .the_content_wrapper ul, .the_content_wrapper ol {color: #737E86;}.hr_color, .hr_color hr, .hr_dots span {color: #58b32b;background: #58b32b;}.hr_zigzag i {color: #58b32b;}.highlight-left:after,.highlight-right:after {background: #58b32b;}@media only screen and (max-width: 767px) {.highlight-left .wrap:first-child,.highlight-right .wrap:last-child {background: #58b32b;}}#Header .top_bar_left, .header-classic #Top_bar, .header-plain #Top_bar, .header-stack #Top_bar, .header-split #Top_bar,.header-fixed #Top_bar, .header-below #Top_bar, #Header_creative, #Top_bar #menu, .sticky-tb-color #Top_bar.is-sticky {background-color: #052606;}#Top_bar .wpml-languages a.active, #Top_bar .wpml-languages ul.wpml-lang-dropdown {background-color: #052606;}#Top_bar .top_bar_right:before {background-color: #052606;}#Header .top_bar_right {background-color: #052606;}#Top_bar .top_bar_right a:not(.action_button) {color: #444444;}#Top_bar .menu > li > a,#Top_bar #menu ul li.submenu .menu-toggle {color: #ffffff;}#Top_bar .menu > li.current-menu-item > a,#Top_bar .menu > li.current_page_item > a,#Top_bar .menu > li.current-menu-parent > a,#Top_bar .menu > li.current-page-parent > a,#Top_bar .menu > li.current-menu-ancestor > a,#Top_bar .menu > li.current-page-ancestor > a,#Top_bar .menu > li.current_page_ancestor > a,#Top_bar .menu > li.hover > a {color: #91D56E;}#Top_bar .menu > li a:after {background: #91D56E;}.menuo-arrows #Top_bar .menu > li.submenu > a > span:not(.description)::after {border-top-color: #ffffff;}#Top_bar .menu > li.current-menu-item.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current_page_item.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-menu-parent.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-page-parent.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-menu-ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current-page-ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.current_page_ancestor.submenu > a > span:not(.description)::after,#Top_bar .menu > li.hover.submenu > a > span:not(.description)::after {border-top-color: #91D56E;}.menu-highlight #Top_bar #menu > ul > li.current-menu-item > a,.menu-highlight #Top_bar #menu > ul > li.current_page_item > a,.menu-highlight #Top_bar #menu > ul > li.current-menu-parent > a,.menu-highlight #Top_bar #menu > ul > li.current-page-parent > a,.menu-highlight #Top_bar #menu > ul > li.current-menu-ancestor > a,.menu-highlight #Top_bar #menu > ul > li.current-page-ancestor > a,.menu-highlight #Top_bar #menu > ul > li.current_page_ancestor > a,.menu-highlight #Top_bar #menu > ul > li.hover > a {background: #F2F2F2;}.menu-arrow-bottom #Top_bar .menu > li > a:after { border-bottom-color: #91D56E;}.menu-arrow-top #Top_bar .menu > li > a:after {border-top-color: #91D56E;}.header-plain #Top_bar .menu > li.current-menu-item > a,.header-plain #Top_bar .menu > li.current_page_item > a,.header-plain #Top_bar .menu > li.current-menu-parent > a,.header-plain #Top_bar .menu > li.current-page-parent > a,.header-plain #Top_bar .menu > li.current-menu-ancestor > a,.header-plain #Top_bar .menu > li.current-page-ancestor > a,.header-plain #Top_bar .menu > li.current_page_ancestor > a,.header-plain #Top_bar .menu > li.hover > a,.header-plain #Top_bar a:hover#header_cart,.header-plain #Top_bar a:hover#search_button,.header-plain #Top_bar .wpml-languages:hover,.header-plain #Top_bar .wpml-languages ul.wpml-lang-dropdown {background: #F2F2F2;color: #91D56E;}.header-plain #Top_bar,.header-plain #Top_bar .menu > li > a span:not(.description),.header-plain #Top_bar a#header_cart,.header-plain #Top_bar a#search_button,.header-plain #Top_bar .wpml-languages,.header-plain #Top_bar a.button.action_button {border-color: #F2F2F2;}#Top_bar .menu > li ul {background-color: #F2F2F2;}#Top_bar .menu > li ul li a {color: #5f5f5f;}#Top_bar .menu > li ul li a:hover,#Top_bar .menu > li ul li.hover > a {color: #2e2e2e;}#Top_bar .search_wrapper {background: #58b32b;}.overlay-menu-toggle {color: #58b32b !important;background: transparent;}#Overlay {background: rgba(88, 179, 43, 0.95);}#overlay-menu ul li a, .header-overlay .overlay-menu-toggle.focus {color: #ffffff;}#overlay-menu ul li.current-menu-item > a,#overlay-menu ul li.current_page_item > a,#overlay-menu ul li.current-menu-parent > a,#overlay-menu ul li.current-page-parent > a,#overlay-menu ul li.current-menu-ancestor > a,#overlay-menu ul li.current-page-ancestor > a,#overlay-menu ul li.current_page_ancestor > a {color: #bceaa5;}#Top_bar .responsive-menu-toggle,#Header_creative .creative-menu-toggle,#Header_creative .responsive-menu-toggle {color: #58b32b;background: transparent;}#Side_slide{background-color: #191919;border-color: #191919; }#Side_slide,#Side_slide .search-wrapper input.field,#Side_slide a:not(.button),#Side_slide #menu ul li.submenu .menu-toggle{color: #A6A6A6;}#Side_slide a:not(.button):hover,#Side_slide a.active,#Side_slide #menu ul li.hover > .menu-toggle{color: #FFFFFF;}#Side_slide #menu ul li.current-menu-item > a,#Side_slide #menu ul li.current_page_item > a,#Side_slide #menu ul li.current-menu-parent > a,#Side_slide #menu ul li.current-page-parent > a,#Side_slide #menu ul li.current-menu-ancestor > a,#Side_slide #menu ul li.current-page-ancestor > a,#Side_slide #menu ul li.current_page_ancestor > a,#Side_slide #menu ul li.hover > a,#Side_slide #menu ul li:hover > a{color: #FFFFFF;}#Action_bar .contact_details{color: #bbbbbb}#Action_bar .contact_details a{color: #0095eb}#Action_bar .contact_details a:hover{color: #007cc3}#Action_bar .social li a,#Header_creative .social li a,#Action_bar .social-menu a{color: #bbbbbb}#Action_bar .social li a:hover,#Header_creative .social li a:hover,#Action_bar .social-menu a:hover{color: #FFFFFF}#Subheader .title{color: #ffffff;}#Subheader ul.breadcrumbs li, #Subheader ul.breadcrumbs li a{color: rgba(255, 255, 255, 0.6);}#Footer, #Footer .widget_recent_entries ul li a {color: #ffffff;}#Footer a {color: #58b32b;}#Footer a:hover {color: #459e19;}#Footer h1, #Footer h1 a, #Footer h1 a:hover,#Footer h2, #Footer h2 a, #Footer h2 a:hover,#Footer h3, #Footer h3 a, #Footer h3 a:hover,#Footer h4, #Footer h4 a, #Footer h4 a:hover,#Footer h5, #Footer h5 a, #Footer h5 a:hover,#Footer h6, #Footer h6 a, #Footer h6 a:hover {color: #ffffff;}#Footer .themecolor, #Footer .widget_meta ul, #Footer .widget_pages ul, #Footer .widget_rss ul, #Footer .widget_mfn_recent_comments ul li:after, #Footer .widget_archive ul,#Footer .widget_recent_comments ul li:after, #Footer .widget_nav_menu ul, #Footer .widget_price_filter .price_label .from, #Footer .widget_price_filter .price_label .to,#Footer .star-rating span {color: #58b32b;}#Footer .themebg, #Footer .widget_categories ul, #Footer .Recent_posts ul li .desc:after, #Footer .Recent_posts ul li .photo .c,#Footer .widget_recent_entries ul li:after, #Footer .widget_mfn_menu ul li a:hover, #Footer .widget_product_categories ul {background-color: #58b32b;}#Footer .Recent_posts ul li a .desc .date, #Footer .widget_recent_entries ul li .post-date, #Footer .tp_recent_tweets .twitter_time,#Footer .widget_price_filter .price_label, #Footer .shop-filters .woocommerce-result-count, #Footer ul.product_list_widget li .quantity,#Footer .widget_shopping_cart ul.product_list_widget li dl {color: #72a07f;}#Footer .footer_copy .social li a,#Footer .footer_copy .social-menu a{color: #65666C;}#Footer .footer_copy .social li a:hover,#Footer .footer_copy .social-menu a:hover{color: #FFFFFF;}a#back_to_top.button.button_js,#popup_contact > a.button{color: #65666C;background:transparent;-webkit-box-shadow:none;box-shadow:none;}a#back_to_top.button.button_js:after,#popup_contact > a.button:after{display:none;}#Sliding-top, #Sliding-top .widget_recent_entries ul li a {color: #ffffff;}#Sliding-top a {color: #58b32b;}#Sliding-top a:hover {color: #459e19;}#Sliding-top h1, #Sliding-top h1 a, #Sliding-top h1 a:hover,#Sliding-top h2, #Sliding-top h2 a, #Sliding-top h2 a:hover,#Sliding-top h3, #Sliding-top h3 a, #Sliding-top h3 a:hover,#Sliding-top h4, #Sliding-top h4 a, #Sliding-top h4 a:hover,#Sliding-top h5, #Sliding-top h5 a, #Sliding-top h5 a:hover,#Sliding-top h6, #Sliding-top h6 a, #Sliding-top h6 a:hover {color: #ffffff;}#Sliding-top .themecolor, #Sliding-top .widget_meta ul, #Sliding-top .widget_pages ul, #Sliding-top .widget_rss ul, #Sliding-top .widget_mfn_recent_comments ul li:after, #Sliding-top .widget_archive ul,#Sliding-top .widget_recent_comments ul li:after, #Sliding-top .widget_nav_menu ul, #Sliding-top .widget_price_filter .price_label .from, #Sliding-top .widget_price_filter .price_label .to,#Sliding-top .star-rating span {color: #58b32b;}#Sliding-top .themebg, #Sliding-top .widget_categories ul, #Sliding-top .Recent_posts ul li .desc:after, #Sliding-top .Recent_posts ul li .photo .c,#Sliding-top .widget_recent_entries ul li:after, #Sliding-top .widget_mfn_menu ul li a:hover, #Sliding-top .widget_product_categories ul {background-color: #58b32b;}#Sliding-top .Recent_posts ul li a .desc .date, #Sliding-top .widget_recent_entries ul li .post-date, #Sliding-top .tp_recent_tweets .twitter_time,#Sliding-top .widget_price_filter .price_label, #Sliding-top .shop-filters .woocommerce-result-count, #Sliding-top ul.product_list_widget li .quantity,#Sliding-top .widget_shopping_cart ul.product_list_widget li dl {color: #72a07f;}blockquote, blockquote a, blockquote a:hover {color: #444444;}.image_frame .image_wrapper .image_links,.portfolio_group.masonry-hover .portfolio-item .masonry-hover-wrapper .hover-desc {background: rgba(88, 179, 43, 0.8);}.masonry.tiles .post-item .post-desc-wrapper .post-desc .post-title:after,.masonry.tiles .post-item.no-img,.masonry.tiles .post-item.format-quote,.blog-teaser li .desc-wrapper .desc .post-title:after,.blog-teaser li.no-img,.blog-teaser li.format-quote {background: #58b32b;}.image_frame .image_wrapper .image_links a {color: #ffffff;}.image_frame .image_wrapper .image_links a:hover {background: #ffffff;color: #58b32b;}.image_frame {border-color: #f8f8f8;}.image_frame .image_wrapper .mask::after {background: rgba(255, 255, 255, 0.4);}.sliding_box .desc_wrapper {background: #58b32b;}.sliding_box .desc_wrapper:after {border-bottom-color: #58b32b;}.counter .icon_wrapper i {color: #58b32b;}.quick_fact .number-wrapper {color: #58b32b;}.progress_bars .bars_list li .bar .progress {background-color: #58b32b;}a:hover.icon_bar {color: #58b32b !important;}a.content_link, a:hover.content_link {color: #58b32b;}a.content_link:before {border-bottom-color: #58b32b;}a.content_link:after {border-color: #58b32b;}.get_in_touch, .infobox {background-color: #58b32b;}.google-map-contact-wrapper .get_in_touch:after {border-top-color: #58b32b;}.timeline_items li h3:before,.timeline_items:after,.timeline .post-item:before {border-color: #58b32b;}.how_it_works .image .number {background: #58b32b;}.trailer_box .desc .subtitle,.trailer_box.plain .desc .line {background-color: #58b32b;}.trailer_box.plain .desc .subtitle {color: #58b32b;}.icon_box .icon_wrapper, .icon_box a .icon_wrapper,.style-simple .icon_box:hover .icon_wrapper {color: #58b32b;}.icon_box:hover .icon_wrapper:before,.icon_box a:hover .icon_wrapper:before {background-color: #58b32b;}ul.clients.clients_tiles li .client_wrapper:hover:before {background: #58b32b;}ul.clients.clients_tiles li .client_wrapper:after {border-bottom-color: #58b32b;}.list_item.lists_1 .list_left {background-color: #58b32b;}.list_item .list_left {color: #58b32b;}.feature_list ul li .icon i {color: #58b32b;}.feature_list ul li:hover,.feature_list ul li:hover a {background: #58b32b;}.ui-tabs .ui-tabs-nav li.ui-state-active a,.accordion .question.active .title > .acc-icon-plus,.accordion .question.active .title > .acc-icon-minus,.faq .question.active .title > .acc-icon-plus,.faq .question.active .title,.accordion .question.active .title {color: #58b32b;}.ui-tabs .ui-tabs-nav li.ui-state-active a:after {background: #58b32b;}body.table-hover:not(.woocommerce-page) table tr:hover td {background: #58b32b;}.pricing-box .plan-header .price sup.currency,.pricing-box .plan-header .price > span {color: #58b32b;}.pricing-box .plan-inside ul li .yes {background: #58b32b;}.pricing-box-box.pricing-box-featured {background: #58b32b;}input[type="date"], input[type="email"], input[type="number"], input[type="password"], input[type="search"], input[type="tel"], input[type="text"], input[type="url"],select, textarea, .woocommerce .quantity input.qty,.dark input[type="email"],.dark input[type="password"],.dark input[type="tel"],.dark input[type="text"],.dark select,.dark textarea{color: #626262;background-color: rgba(236, 239, 241, 1);border-color: #eceff1;}::-webkit-input-placeholder {color: #929292;}::-moz-placeholder {color: #929292;}:-ms-input-placeholder {color: #929292;}input[type="date"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="password"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="text"]:focus, input[type="url"]:focus, select:focus, textarea:focus {color: #58b32b;background-color: rgba(236, 239, 241, 1) !important;border-color: #58b32b;}:focus::-webkit-input-placeholder {color: #929292;}:focus::-moz-placeholder {color: #929292;}.woocommerce span.onsale, .shop_slider .shop_slider_ul li .item_wrapper span.onsale {border-top-color: #58b32b !important;}.woocommerce .widget_price_filter .ui-slider .ui-slider-handle {border-color: #58b32b !important;}@media only screen and ( min-width: 768px ){.header-semi #Top_bar:not(.is-sticky) {background-color: rgba(5, 38, 6, 0.8);}}@media only screen and ( max-width: 767px ){#Top_bar{background: #052606 !important;}#Action_bar{background: #FFFFFF !important;}#Action_bar .contact_details{color: #222222}#Action_bar .contact_details a{color: #0095eb}#Action_bar .contact_details a:hover{color: #007cc3}#Action_bar .social li a,#Action_bar .social-menu a{color: #bbbbbb}#Action_bar .social li a:hover,#Action_bar .social-menu a:hover{color: #777777}}html { background-color: #ffffff;}#Wrapper, #Content { background-color: #ffffff;}body, button, span.date_label, .timeline_items li h3 span, input[type="submit"], input[type="reset"], input[type="button"],input[type="text"], input[type="password"], input[type="tel"], input[type="email"], textarea, select, .offer_li .title h3 {font-family: "Lato", Arial, Tahoma, sans-serif;}#menu > ul > li > a, .action_button, #overlay-menu ul li a {font-family: "Merriweather", Arial, Tahoma, sans-serif;}#Subheader .title {font-family: "Merriweather", Arial, Tahoma, sans-serif;}h1, h2, h3, h4, .text-logo #logo {font-family: "Merriweather", Arial, Tahoma, sans-serif;}h5, h6 {font-family: "Lato", Arial, Tahoma, sans-serif;}blockquote {font-family: "Merriweather", Arial, Tahoma, sans-serif;}.chart_box .chart .num, .counter .desc_wrapper .number-wrapper, .how_it_works .image .number,.pricing-box .plan-header .price, .quick_fact .number-wrapper, .woocommerce .product div.entry-summary .price {font-family: "Merriweather", Arial, Tahoma, sans-serif;}body {font-size: 15px;line-height: 21px;font-weight: 400;letter-spacing: 0px;}big,.big {font-size: 16px;line-height: 28px;font-weight: 400;letter-spacing: 0px;}#menu > ul > li > a, a.button.action_button, #overlay-menu ul li a{font-size: 16px;font-weight: 400;letter-spacing: 0px;}#overlay-menu ul li a{line-height: 24px;}#Subheader .title {font-size: 55px;line-height: 55px;font-weight: 300;letter-spacing: 0px;}h1, .text-logo #logo { font-size: 60px;line-height: 60px;font-weight: 400;letter-spacing: 0px;}h2 { font-size: 48px;line-height: 48px;font-weight: 400;letter-spacing: 0px;}h3 {font-size: 30px;line-height: 32px;font-weight: 400;letter-spacing: 0px;}h4 {font-size: 22px;line-height: 26px;font-weight: 400;letter-spacing: 0px;}h5 {font-size: 18px;line-height: 22px;font-weight: 400;letter-spacing: 0px;}h6 {font-size: 15px;line-height: 19px;font-weight: 400;letter-spacing: 0px;}#Intro .intro-title { font-size: 70px;line-height: 70px;font-weight: 400;letter-spacing: 0px;}@media only screen and (min-width: 768px) and (max-width: 959px){body {font-size: 13px;line-height: 19px;}big,.big {font-size: 14px;line-height: 24px;}#menu > ul > li > a, a.button.action_button, #overlay-menu ul li a {font-size: 14px;}#overlay-menu ul li a{line-height: 21px;}#Subheader .title {font-size: 47px;line-height: 47px;}h1, .text-logo #logo { font-size: 51px;line-height: 51px;}h2 { font-size: 41px;line-height: 41px;}h3 {font-size: 26px;line-height: 27px;}h4 {font-size: 19px;line-height: 22px;}h5 {font-size: 15px;line-height: 19px;}h6 {font-size: 13px;line-height: 19px;}#Intro .intro-title { font-size: 60px;line-height: 60px;}blockquote { font-size: 15px;}.chart_box .chart .num { font-size: 45px; line-height: 45px; }.counter .desc_wrapper .number-wrapper { font-size: 45px; line-height: 45px;}.counter .desc_wrapper .title { font-size: 14px; line-height: 18px;}.faq .question .title { font-size: 14px; }.fancy_heading .title { font-size: 38px; line-height: 38px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 32px; line-height: 32px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 32px; line-height: 32px; }.pricing-box .plan-header h2 { font-size: 27px; line-height: 27px; }.pricing-box .plan-header .price > span { font-size: 40px; line-height: 40px; }.pricing-box .plan-header .price sup.currency { font-size: 18px; line-height: 18px; }.pricing-box .plan-header .price sup.period { font-size: 14px; line-height: 14px;}.quick_fact .number { font-size: 80px; line-height: 80px;}.trailer_box .desc h2 { font-size: 27px; line-height: 27px; }.widget > h3 { font-size: 17px; line-height: 20px; }}@media only screen and (min-width: 480px) and (max-width: 767px){body {font-size: 13px;line-height: 19px;}big,.big {font-size: 13px;line-height: 21px;}#menu > ul > li > a, a.button.action_button, #overlay-menu ul li a {font-size: 13px;}#overlay-menu ul li a{line-height: 19.5px;}#Subheader .title {font-size: 41px;line-height: 41px;}h1, .text-logo #logo { font-size: 45px;line-height: 45px;}h2 { font-size: 36px;line-height: 36px;}h3 {font-size: 23px;line-height: 24px;}h4 {font-size: 17px;line-height: 20px;}h5 {font-size: 14px;line-height: 19px;}h6 {font-size: 13px;line-height: 19px;}#Intro .intro-title { font-size: 53px;line-height: 53px;}blockquote { font-size: 14px;}.chart_box .chart .num { font-size: 40px; line-height: 40px; }.counter .desc_wrapper .number-wrapper { font-size: 40px; line-height: 40px;}.counter .desc_wrapper .title { font-size: 13px; line-height: 16px;}.faq .question .title { font-size: 13px; }.fancy_heading .title { font-size: 34px; line-height: 34px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 28px; line-height: 28px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 28px; line-height: 28px; }.pricing-box .plan-header h2 { font-size: 24px; line-height: 24px; }.pricing-box .plan-header .price > span { font-size: 34px; line-height: 34px; }.pricing-box .plan-header .price sup.currency { font-size: 16px; line-height: 16px; }.pricing-box .plan-header .price sup.period { font-size: 13px; line-height: 13px;}.quick_fact .number { font-size: 70px; line-height: 70px;}.trailer_box .desc h2 { font-size: 24px; line-height: 24px; }.widget > h3 { font-size: 16px; line-height: 19px; }}@media only screen and (max-width: 479px){body {font-size: 13px;line-height: 19px;}big,.big {font-size: 13px;line-height: 19px;}#menu > ul > li > a, a.button.action_button, #overlay-menu ul li a {font-size: 13px;}#overlay-menu ul li a{line-height: 19.5px;}#Subheader .title {font-size: 33px;line-height: 33px;}h1, .text-logo #logo { font-size: 36px;line-height: 36px;}h2 { font-size: 29px;line-height: 29px;}h3 {font-size: 18px;line-height: 19px;}h4 {font-size: 13px;line-height: 19px;}h5 {font-size: 13px;line-height: 19px;}h6 {font-size: 13px;line-height: 19px;}#Intro .intro-title { font-size: 42px;line-height: 42px;}blockquote { font-size: 13px;}.chart_box .chart .num { font-size: 35px; line-height: 35px; }.counter .desc_wrapper .number-wrapper { font-size: 35px; line-height: 35px;}.counter .desc_wrapper .title { font-size: 13px; line-height: 26px;}.faq .question .title { font-size: 13px; }.fancy_heading .title { font-size: 30px; line-height: 30px; }.offer .offer_li .desc_wrapper .title h3 { font-size: 26px; line-height: 26px; }.offer_thumb_ul li.offer_thumb_li .desc_wrapper .title h3 {font-size: 26px; line-height: 26px; }.pricing-box .plan-header h2 { font-size: 21px; line-height: 21px; }.pricing-box .plan-header .price > span { font-size: 32px; line-height: 32px; }.pricing-box .plan-header .price sup.currency { font-size: 14px; line-height: 14px; }.pricing-box .plan-header .price sup.period { font-size: 13px; line-height: 13px;}.quick_fact .number { font-size: 60px; line-height: 60px;}.trailer_box .desc h2 { font-size: 21px; line-height: 21px; }.widget > h3 { font-size: 15px; line-height: 18px; }}.with_aside .sidebar.columns {width: 23%;}.with_aside .sections_group {width: 77%;}.aside_both .sidebar.columns {width: 18%;}.aside_both .sidebar.sidebar-1{ margin-left: -82%;}.aside_both .sections_group {width: 64%;margin-left: 18%;}@media only screen and (min-width:1240px){#Wrapper, .with_aside .content_wrapper {max-width: 1240px;}.section_wrapper, .container {max-width: 1220px;}.layout-boxed.header-boxed #Top_bar.is-sticky{max-width: 1240px;}}@media only screen and (max-width: 767px){.section_wrapper,.container,.four.columns .widget-area { max-width: 700px !important; }}#Top_bar #logo,.header-fixed #Top_bar #logo,.header-plain #Top_bar #logo,.header-transparent #Top_bar #logo {height: 60px;line-height: 60px;padding: 15px 0;}.logo-overflow #Top_bar:not(.is-sticky) .logo {height: 90px;}#Top_bar .menu > li > a {padding: 15px 0;}.menu-highlight:not(.header-creative) #Top_bar .menu > li > a {margin: 20px 0;}.header-plain:not(.menu-highlight) #Top_bar .menu > li > a span:not(.description) {line-height: 90px;}.header-fixed #Top_bar .menu > li > a {padding: 30px 0;}#Top_bar .top_bar_right,.header-plain #Top_bar .top_bar_right {height: 90px;}#Top_bar .top_bar_right_wrapper { top: 25px;}.header-plain #Top_bar a#header_cart, .header-plain #Top_bar a#search_button,.header-plain #Top_bar .wpml-languages,.header-plain #Top_bar a.button.action_button {line-height: 90px;}.header-plain #Top_bar .wpml-languages,.header-plain #Top_bar a.button.action_button {height: 90px;}@media only screen and (max-width: 767px){#Top_bar a.responsive-menu-toggle { top: 40px;}.mobile-header-mini #Top_bar #logo{height:50px!important;line-height:50px!important;margin:5px 0;}}.twentytwenty-before-label::before { content: "Before";}.twentytwenty-after-label::before { content: "After";}#Side_slide{right:-250px;width:250px;}.blog-teaser li .desc-wrapper .desc{background-position-y:-1px;}
</style>
<!-- style | custom css | theme options -->
<style id="mfn-dnmc-theme-css">
h2.entry-title { font-size: 180%; line-height: 120%; }

</style>
<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.7.2 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){                                 
                        try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
                            if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})                  
                        }catch(d){console.log("Failure at Presize of Slider:"+d)}                       
                    };</script>

<!-- Easy FancyBox 1.6.2 using FancyBox 1.3.8 - RavanH (http://status301.net/wordpress-plugins/easy-fancybox/) -->
<script type="text/javascript">
/* <![CDATA[ */
var fb_timeout = null;
var fb_opts = { 'overlayShow' : true, 'hideOnOverlayClick' : true, 'showCloseButton' : true, 'margin' : 20, 'centerOnScroll' : true, 'enableEscapeButton' : true, 'autoScale' : true };
var easy_fancybox_handler = function(){
    /* IMG */
    var fb_IMG_select = 'a[href*=".jpg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpg"]:not(.nolightbox), a[href*=".jpeg"]:not(.nolightbox,li.nolightbox>a), area[href*=".jpeg"]:not(.nolightbox), a[href*=".png"]:not(.nolightbox,li.nolightbox>a), area[href*=".png"]:not(.nolightbox), a[href*=".webp"]:not(.nolightbox,li.nolightbox>a), area[href*=".webp"]:not(.nolightbox)';
    jQuery(fb_IMG_select).addClass('fancybox image');
    var fb_IMG_sections = jQuery('div.gallery ');
    fb_IMG_sections.each(function() { jQuery(this).find(fb_IMG_select).attr('rel', 'gallery-' + fb_IMG_sections.index(this)); });
    jQuery('a.fancybox, area.fancybox, li.fancybox a').fancybox( jQuery.extend({}, fb_opts, { 'transitionIn' : 'elastic', 'easingIn' : 'easeOutBack', 'transitionOut' : 'elastic', 'easingOut' : 'easeInBack', 'opacity' : false, 'hideOnContentClick' : false, 'titleShow' : true, 'titlePosition' : 'over', 'titleFromAlt' : true, 'showNavArrows' : true, 'enableKeyboardNav' : true, 'cyclic' : false }) );
}
var easy_fancybox_auto = function(){
    /* Auto-click */
    setTimeout(function(){jQuery('#fancybox-auto').trigger('click')},1000);
}
/* ]]> */
</script>
<noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript><!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118896585-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-118896585-1');
</script>

    </head>

<!-- body -->
<body class="home page-template-default page page-id-2310 template-slider  color-custom style-simple button-flat layout-full-width no-content-padding header-fixed minimalist-header-no sticky-header sticky-white ab-hide subheader-both-center menu-line-below-80 menuo-right menuo-no-borders mobile-tb-hide mobile-mini-mr-ll tr-menu be-reg-20882 wpb-js-composer js-comp-ver-5.4.7 vc_responsive">

    <!-- mfn_hook_top --><!-- mfn_hook_top -->
    
    
    <!-- #Wrapper -->
    <div id="Wrapper">

        
        
        <!-- #Header_bg -->
        <div id="Header_wrapper"  class="bg-parallax" data-enllax-ratio="0.3">

            <!-- #Header -->
            <header id="Header">
                


<!-- .header_placeholder 4sticky  -->
<div class="header_placeholder"></div>

<div id="Top_bar" class="loading">

    <div class="container">
        <div class="column one">
        
            <div class="top_bar_left clearfix">
            
                <!-- Logo -->
                <div class="logo"><a id="logo" href="https://demos.mictronicx.com/shajar-e-hayat" title="Splendid Logo" data-height="60" data-padding="15"><img class="logo-main scale-with-grid" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/eco.png" data-retina="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/retina-eco.png" data-height="29" alt="eco" /><img class="logo-sticky scale-with-grid" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/eco.png" data-retina="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/retina-eco.png" data-height="29" alt="eco" /><img class="logo-mobile scale-with-grid" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/eco.png" data-retina="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/retina-eco.png" data-height="29" alt="eco" /><img class="logo-mobile-sticky scale-with-grid" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/eco.png" data-retina="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2016/04/retina-eco.png" data-height="29" alt="eco" /></a></div>          
                <div class="menu_wrapper">
                    <nav id="menu"><ul id="menu-main-menu" class="menu menu-main"><li id="menu-item-2311" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2310 current_page_item"><a href="https://demos.mictronicx.com/shajar-e-hayat/"><span>Home</span></a></li>
<li id="menu-item-2315" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://demos.mictronicx.com/shajar-e-hayat/programs/"><span>Programs</span></a></li>
<li id="menu-item-2314" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://demos.mictronicx.com/shajar-e-hayat/the-company/"><span>The company</span></a></li>
<li id="menu-item-2313" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://demos.mictronicx.com/shajar-e-hayat/articles-and-researches/"><span>Articles and researches</span></a></li>
<li id="menu-item-2312" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://demos.mictronicx.com/shajar-e-hayat/contact-us/"><span>Contact us</span></a></li>
<li id="menu-item-2368" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children"><a href="{{'login'}}"><span>Login</span></a>
</li>
</ul></nav><a class="responsive-menu-toggle " href="#"><i class="icon-menu-fine"></i></a>                   
                </div>          
                
                <div class="secondary_menu_wrapper">
                    <!-- #secondary-menu -->
                                    </div>
                
                <div class="banner_wrapper">
                                    </div>
                
                <div class="search_wrapper">
                    <!-- #searchform -->
                    
                    
<form method="get" id="searchform" action="https://demos.mictronicx.com/shajar-e-hayat/">
                        
        
    <i class="icon_search icon-search-fine"></i>
    <a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>
    
    <input type="text" class="field" name="s" placeholder="Enter your search" />            
    <input type="submit" class="submit" value="" style="display:none;" />
    
</form>                 
                </div>              
                
            </div>
            
            <div class="top_bar_right"><div class="top_bar_right_wrapper"><a href="http://themeforest.net/item/betheme-responsive-multipurpose-wordpress-theme/7758048?ref=muffingroup" class="button button_theme button_js action_button " target="_blank"><span class="button_label">Buy now</span></a></div></div>          
        </div>
    </div>
</div>              <div class="mfn-main-slider" id="mfn-rev-slider"><link href="https://fonts.googleapis.com/css?family=Merriweather:300%7CLato:300" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div id="rev_slider_13_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.7.2 auto mode -->
    <div id="rev_slider_13_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7.2">
<ul>    <!-- SLIDE  -->
    <li data-index="rs-28" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
        <!-- MAIN IMAGE -->
        <img src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/revslider/eco/home_eco_sliderbg.jpg"  alt="" title="Home"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="6" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption mfnrs_eco_large   tp-resizeme rs-parallaxlevel-3" 
             id="slide-28-layer-1" 
             data-x="center" data-hoffset="" 
             data-y="center" data-voffset="-120" 
                        data-width="['auto']"
            data-height="['auto']"
 
            data-type="text" 
            data-responsive_offset="on" 

            data-frames='[{"from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","speed":1500,"to":"o:1;","delay":500,"ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['center','center','center','center']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 5; white-space: nowrap; color: rgba(255,255,255,1);">Nature always<br />wears the colors of the spirit </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption mfnrs_eco_medium   tp-resizeme rs-parallaxlevel-2" 
             id="slide-28-layer-2" 
             data-x="center" data-hoffset="" 
             data-y="center" data-voffset="30" 
                        data-width="['auto']"
            data-height="['auto']"
 
            data-type="text" 
            data-responsive_offset="on" 

            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 6; white-space: nowrap; color: rgba(255,255,255,1);">Duis sed odio sit amet nibh vulputate cursus a sit amet mauris morbi accumsan ipsum velit </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1" 
             id="slide-28-layer-3" 
             data-x="center" data-hoffset="" 
             data-y="bottom" data-voffset="50" 
                        data-width="['none','none','none','none']"
            data-height="['none','none','none','none']"
 
            data-type="image" 
            data-responsive_offset="on" 

            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[0,0,0,0]"
            data-paddingright="[0,0,0,0]"
            data-paddingbottom="[0,0,0,0]"
            data-paddingleft="[0,0,0,0]"

            style="z-index: 7;"><img src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/revslider/eco/home_eco_slider_sep.png" alt="" data-ww="2px" data-hh="119px" width="2" height="119" data-no-retina> </div>

        <!-- LAYER NR. 4 -->
        <a class="tp-caption mfnrs_eco_button   tp-resizeme rs-parallaxlevel-3" 
 href="#" target="_self"             id="slide-28-layer-4" 
             data-x="center" data-hoffset="" 
             data-y="center" data-voffset="140" 
                        data-width="['auto']"
            data-height="['auto']"
 
            data-type="text" 
            data-actions=''
            data-responsive_offset="on" 

            data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];s:inherit;e:inherit;","speed":2000,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255,255,255,1);bg:rgba(71,155,30,1);br:0px 0px 0px 0px;"}]'
            data-textAlign="['left','left','left','left']"
            data-paddingtop="[15,15,15,15]"
            data-paddingright="[40,40,40,40]"
            data-paddingbottom="[15,15,15,15]"
            data-paddingleft="[40,40,40,40]"

            style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; color: rgba(255,255,255,1);background-color:rgba(88,179,43,1);text-decoration: none;">Vestibulum ante nullam </a>
    </li>
</ul>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div> </div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss=".tp-caption.mfnrs_eco_large,.mfnrs_eco_large{color:#ffffff;font-size:70px;line-height:70px;font-weight:300;font-style:normal;font-family:Merriweather;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.mfnrs_eco_medium,.mfnrs_eco_medium{color:#ffffff;font-size:24px;line-height:28px;font-weight:300;font-style:normal;font-family:Lato;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.mfnrs_eco_button,.mfnrs_eco_button{color:#ffffff;font-size:20px;line-height:22px;font-weight:300;font-style:normal;font-family:Merriweather;text-decoration:none;background-color:#58b32b;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px}.tp-caption.mfnrs_eco_button:hover,.mfnrs_eco_button:hover{color:rgba(255,255,255,1.00);text-decoration:none;background-color:rgba(71,155,30,1.00);border-color:transparent;border-style:none;border-width:0px;border-radius:0px 0px 0px 0px}";
                if(htmlDiv) {
                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                }else{
                    var htmlDiv = document.createElement("div");
                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                }
            </script>
        <script type="text/javascript">
if (setREVStartSize!==undefined) setREVStartSize(
    {c: '#rev_slider_13_1', gridwidth: [1240], gridheight: [868], sliderLayout: 'auto'});
            
var revapi13,
    tpj;    
(function() {           
    if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded",onLoad); else onLoad();  
    function onLoad() {             
        if (tpj===undefined) { tpj = jQuery; if("off" == "on") tpj.noConflict();}
    if(tpj("#rev_slider_13_1").revolution == undefined){
        revslider_showDoubleJqueryError("#rev_slider_13_1");
    }else{
        revapi13 = tpj("#rev_slider_13_1").show().revolution({
            sliderType:"hero",
            jsFileLocation:"//demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/revslider/public/assets/js/",
            sliderLayout:"auto",
            dottedOverlay:"none",
            delay:9000,
            visibilityLevels:[1240,1024,778,480],
            gridwidth:1240,
            gridheight:868,
            lazyType:"none",
            parallax: {
                type:"scroll",
                origo:"enterpoint",
                speed:400,
                speedbg:0,
                speedls:0,
                levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
            },
            shadow:0,
            spinner:"spinner2",
            autoHeight:"off",
            disableProgressBar:"on",
            hideThumbsOnMobile:"off",
            hideSliderAtLimit:0,
            hideCaptionAtLimit:0,
            hideAllCaptionAtLilmit:0,
            debugMode:false,
            fallbacks: {
                simplifyAll:"off",
                disableFocusListener:false,
            }
        });
    }; /* END OF revapi call */
    
 }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */
</script>
        <script>
                    var htmlDivCss = ' #rev_slider_13_1_wrapper .tp-loader.spinner2{ background-color: #FFFFFF !important; } ';
                    var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                    if(htmlDiv) {
                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                    }
                    else{
                        var htmlDiv = document.createElement('div');
                        htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                        document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                    }
                    </script>
                    </div><!-- END REVOLUTION SLIDER --></div>          </header>

            
        </div>

        
        <!-- mfn_hook_content_before --><!-- mfn_hook_content_before -->    
<!-- #Content -->
<div id="Content">
    <div class="content_wrapper clearfix">

        <!-- .sections_group -->
        <div class="sections_group">
        
            <div class="entry-content" itemprop="mainContentOfPage">
            
                <div class="section mcb-section   "  style="padding-top:30px; padding-bottom:0px; background-color:#f0eee4" ><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  column-margin-30px valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix align_center"  style=""><h4 style="margin: 0;">Donec cursus amet et sollicitudin sodales</h4></div></div></div></div></div></div><div class="section mcb-section   "  style="padding-top:80px; padding-bottom:40px; background-color:" ><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one-third column_icon_box "><div class="animate" data-anim-type="fadeInUp"><div class="icon_box icon_position_top no_border"><div class="image_wrapper"><img src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_iconbox1.png" class="scale-with-grid" alt="" width="" height=""/></div><div class="desc_wrapper"><h4 class="title">Vivamus blandit</h4><div class="desc">Sed ut interdum lectus, eu ultrices mauris. Aenean eu massa lobortis commodo</div></div></div>
</div>
</div><div class="column mcb-column one-third column_icon_box "><div class="animate" data-anim-type="fadeInUp"><div class="icon_box icon_position_top no_border"><div class="image_wrapper"><img src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_iconbox2.png" class="scale-with-grid" alt="" width="" height=""/></div><div class="desc_wrapper"><h4 class="title">Proin enim cras</h4><div class="desc">Aliquam eleifend magna a mauris lacinia, ut aliquet elit feugiat. Donec a dapibus dolor</div></div></div>
</div>
</div><div class="column mcb-column one-third column_icon_box "><div class="animate" data-anim-type="fadeInUp"><div class="icon_box icon_position_top no_border"><div class="image_wrapper"><img src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_iconbox3.png" class="scale-with-grid" alt="" width="" height=""/></div><div class="desc_wrapper"><h4 class="title">Etiam venenatis</h4><div class="desc">Curabitur ultrices dui sit amet varius tincidunt. Nullam ullamcorper ipsum condimentum</div></div></div>
</div>
</div></div></div></div></div><div class="section mcb-section equal-height-wrap full-width "  style="padding-top:0px; padding-bottom:0px; background-color:#ccc" ><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap three-fifth  valign-top clearfix" style="background-color:#093725; background-image:url(http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_wrapbg1.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:; background-size:; -webkit-background-size:"  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_divider "><hr class="no_line" style="margin: 0 auto 400px;"/>
</div></div></div><div class="wrap mcb-wrap two-fifth  column-margin-30px valign-top clearfix" style="padding:80px 50px 65px; background-color:#539a36; background-image:url(http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_section_box.png); background-repeat:no-repeat; background-position:center top; background-attachment:; background-size:; -webkit-background-size:"  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix"  style=""><h2 style="color: #fff; word-wrap: break-word">Ways of protecting<br />the environment</h2>
<hr class="no_line" style="margin: 0 auto 10px;"/>

<div class="image_frame image_item no_link scale-with-grid alignnone no_border" ><div class="image_wrapper"><img class="scale-with-grid" src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_sep1.png" alt=""   /></div></div>

<hr class="no_line" style="margin: 0 auto 25px;"/>

<h5 style="color: #fff;">Donec blandit luctus laoreet. Donec dui arcu dapibus</h5></div></div><div class="column mcb-column one column_column "><div class="column_attr clearfix"  style=" background-image:url('http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_list_num1.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;"><p class="big" style="color: #fff; margin: 0; font-weight: 300;">Etiam finibus auctor sagittis. Phasellus ac tempor odio. Donec scelerisque ullamcorper felis, id sagittis dolor.</p></div></div><div class="column mcb-column one column_column "><div class="column_attr clearfix"  style=" background-image:url('http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_list_num2.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;"><p class="big" style="color: #fff; margin: 0; font-weight: 300;">Nunc tempus porttitor ipsum, quis cursus nisl fringilla non. Cras id eros molestie, iaculis dolor eu, vulputate tellus velit.</p></div></div><div class="column mcb-column one column_column "><div class="column_attr clearfix"  style=" background-image:url('http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_list_num3.png'); background-repeat:no-repeat; background-position:left top; padding:5px 0 0 70px;"><p class="big" style="color: #fff; margin: 0; font-weight: 300;">Proin id molestie lorem, sed venenatis orci. Curabitur a lorem sed sem bibendum porttitor varius sit amet sem.</p></div></div><div class="column mcb-column one column_button"><a class="button  button_size_3 button_js" href="http://splendidlogo.com/the-company/" 0    ><span class="button_label">Read more about us</span></a>
</div></div></div></div></div><div class="section mcb-section   "  style="padding-top:80px; padding-bottom:240px; background-color:; background-image:url(http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_sectionbg1.jpg); background-repeat:no-repeat; background-position:center bottom; background-attachment:; background-size:; -webkit-background-size:" ><div class="section_wrapper mcb-section-inner"><div class="wrap mcb-wrap one  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix align_center"  style=" padding:0 8%;"><h2>Fusce rutrum volutpat faucibus</h2>
<p class="big">Onceptos himenaeos. Mauris in erat justo. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim.</p></div></div><div class="column mcb-column one column_blog_slider "><div class="blog_slider clearfix  flat" data-count="3"><div class="blog_slider_header"></div><ul class="blog_slider_ul"><li class="post-1 post type-post status-publish format-standard hentry category-uncategorized"><div class="item_wrapper"><div class="image_frame scale-with-grid"><div class="image_wrapper"><a href="https://demos.mictronicx.com/shajar-e-hayat/hello-world/"></a></div></div><div class="date_label">June 13, 2018</div><div class="desc"><h4><a href="https://demos.mictronicx.com/shajar-e-hayat/hello-world/">Hello world!</a></h4><hr class="hr_color" /><a href="https://demos.mictronicx.com/shajar-e-hayat/hello-world/" class="button button_left button_js"><span class="button_icon"><i class="icon-layout"></i></span><span class="button_label">Read more</span></a></div></div></li><li class="post-2277 post type-post status-publish format-standard has-post-thumbnail hentry category-news category-subjects"><div class="item_wrapper"><div class="image_frame scale-with-grid"><div class="image_wrapper"><a href="https://demos.mictronicx.com/shajar-e-hayat/vestibulum-commodo-volutpat-laoreet/"><img width="960" height="750" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog1-960x750.jpg" class="scale-with-grid wp-post-image" alt="" /></a></div></div><div class="date_label">May 8, 2014</div><div class="desc"><h4><a href="https://demos.mictronicx.com/shajar-e-hayat/vestibulum-commodo-volutpat-laoreet/">Vestibulum commodo volutpat laoreet</a></h4><hr class="hr_color" /><a href="https://demos.mictronicx.com/shajar-e-hayat/vestibulum-commodo-volutpat-laoreet/" class="button button_left button_js"><span class="button_icon"><i class="icon-layout"></i></span><span class="button_label">Read more</span></a></div></div></li><li class="post-2279 post type-post status-publish format-standard has-post-thumbnail hentry category-news category-subjects"><div class="item_wrapper"><div class="image_frame scale-with-grid"><div class="image_wrapper"><a href="https://demos.mictronicx.com/shajar-e-hayat/quisque-lorem-tortor-fringilla-bulum/"><img width="960" height="750" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog2-960x750.jpg" class="scale-with-grid wp-post-image" alt="" /></a></div></div><div class="date_label">May 7, 2014</div><div class="desc"><h4><a href="https://demos.mictronicx.com/shajar-e-hayat/quisque-lorem-tortor-fringilla-bulum/">Quisque lorem tortor fringilla bulum</a></h4><hr class="hr_color" /><a href="https://demos.mictronicx.com/shajar-e-hayat/quisque-lorem-tortor-fringilla-bulum/" class="button button_left button_js"><span class="button_icon"><i class="icon-layout"></i></span><span class="button_label">Read more</span></a></div></div></li></ul><div class="slider_pager slider_pagination"></div></div>
</div></div></div><div class="wrap mcb-wrap one-fourth  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_placeholder"><div class="placeholder">&nbsp;</div></div></div></div><div class="wrap mcb-wrap one-second  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column  column-margin-20px"><div class="column_attr clearfix align_center"  style=""><p style="font-size: 13px; color: #A5AEB3;">Fusce ac porttitor elit. In <a href="#">sed feugiat turpis</a>, dictum semper ante. Phasellus luctus, sapien sit amet ullamcorper ultrices, tortor odio hendrerit ante, ut amet.</p></div></div><div class="column mcb-column one column_button"><div class="button_align align_center"><a class="button  button_full_width button_size_3 button_theme button_js" href="http://splendidlogo.com/articles-and-researches/" 0    ><span class="button_label">Read our blog</span></a></div>
</div></div></div><div class="wrap mcb-wrap one  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_divider "><hr class="no_line" style="margin: 0 auto 40px;"/>
</div></div></div><div class="wrap mcb-wrap one-second  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix"  style=""><h4>Vestibulum ornare convallis</h4>
<p class="big">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur et semper libero, quis tristique nisl. Vivamus eget posuere ligula.</p>
<p>Duis et nunc ullamcorper, semper nisl at, tristique neque. Curabitur at rhoncus velit. Etiam pharetra auctor dapibus. Quisque et augue tortor. Pellentesque sed. Sed viverra, felis ut aliquam sagittis, tortor nunc cursus massa, quis tristique ipsum diam ut mi.</p>

</div></div></div></div><div class="wrap mcb-wrap one-second  column-margin-20px valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_image "><div class="animate" data-anim-type="bounceIn"><div class="image_frame image_item no_link scale-with-grid aligncenter no_border" ><div class="image_wrapper"><img class="scale-with-grid" src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_pic1.png" alt=""   /></div></div>
</div>
</div><div class="column mcb-column one column_column "><div class="column_attr clearfix align_center"  style=""><h5>Duis convallis purus a convallis ornare</h5></div></div></div></div><div class="wrap mcb-wrap one  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_divider "><hr class="no_line" style="margin: 0 auto 10px;"/>
</div></div></div><div class="wrap mcb-wrap one-second  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_image "><div class="image_frame image_item no_link scale-with-grid aligncenter no_border" ><div class="image_wrapper"><img class="scale-with-grid" src="http://splendidlogo.com/wp-content/uploads/2016/04/home_eco_pic2.png" alt=""   /></div></div>
</div></div></div><div class="wrap mcb-wrap one-second  valign-top clearfix" style=""  ><div class="mcb-wrap-inner"><div class="column mcb-column one column_column "><div class="column_attr clearfix align_center"  style=""><p class="big" style="color: #879298;">Nam pharetra urna non interdum feugiat</p>
<div class="progress_icons" data-active="7" data-color=""><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span><span class="progress_icon"><i class="icon-light-up"></i></span></div>

<p class="big" style="color: #879298;">Etiam eget velit non mi auctor semper sit amet</p>
<div class="progress_icons" data-active="5" data-color=""><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span><span class="progress_icon"><i class="icon-leaf"></i></span></div>
</div></div></div></div></div></div><div class="section the_content no_content"><div class="section_wrapper"><div class="the_content_wrapper"></div></div></div>                
                <div class="section section-page-footer">
                    <div class="section_wrapper clearfix">
                    
                        <div class="column one page-pager">
                                                    </div>
                        
                    </div>
                </div>
                
            </div>
            
                
        </div>
        
        <!-- .four-columns - sidebar -->
        
    </div>
</div>


<!-- mfn_hook_content_after --><!-- mfn_hook_content_after -->
    <!-- #Footer -->
    <footer id="Footer" class="clearfix">

        
        <div class="widgets_wrapper" style="padding:60px 0;"><div class="container"><div class="column one-fourth"><aside id="text-2" class="widget widget_text">           <div class="textwidget"><div class="image_frame image_item no_link scale-with-grid alignnone no_border" ><div class="image_wrapper"><img class="scale-with-grid" src="http://themes.muffingroup.com/be/eco/wp-content/uploads/2016/04/eco.png" alt=""   /></div></div>


<hr class="no_line" style="margin: 0 auto 20px;"/>


<p>Duis dictum eleifend tortor at scelerisque. Sed elementum augue eu.</p>

<p style="font-size: 28px; line-height: 28px;">
<a style="color: #58b32b;" href="#"><i class="icon-facebook-circled"></i></a>
<a style="color: #58b32b;" href="#"><i class="icon-gplus-circled"></i></a>
<a style="color: #58b32b;" href="#"><i class="icon-twitter-circled"></i></a>
<a style="color: #58b32b;" href="#"><i class="icon-pinterest-circled"></i></a>
<a style="color: #58b32b;" href="#"><i class="icon-linkedin-circled"></i></a>
</p>

<p>Contact us: +61 (0) 3 8376 6284<br> Write us: <a href="mailto:noreply@envato.com">noreply@envato.com</a></p></div>
        </aside><aside id="meta-4" class="widget widget_meta"><h4>Login</h4>            <ul>
            <li><a href="https://demos.mictronicx.com/shajar-e-hayat/sign-up/">Register</a></li>            <li><a href="https://demos.mictronicx.com/shajar-e-hayat/log-in/">Log in</a></li>
            <li><a href="https://demos.mictronicx.com/shajar-e-hayat/feed/">Entries <abbr title="Really Simple Syndication">RSS</abbr></a></li>
            <li><a href="https://demos.mictronicx.com/shajar-e-hayat/comments/feed/">Comments <abbr title="Really Simple Syndication">RSS</abbr></a></li>
            <li><a href="https://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress.org</a></li>           </ul>
            </aside></div><div class="column one-fourth"><aside id="widget_mfn_recent_posts-2" class="widget widget_mfn_recent_posts"><h4>Recent posts</h4><div class="Recent_posts "><ul><li class="post format- no-img"><a href="https://demos.mictronicx.com/shajar-e-hayat/hello-world/"><div class="photo"><span class="c">1</span></div><div class="desc"><h6>Hello world!</h6><span class="date"><i class="icon-clock"></i>June 13, 2018</span></div></a></li><li class="post format-"><a href="https://demos.mictronicx.com/shajar-e-hayat/vestibulum-commodo-volutpat-laoreet/"><div class="photo"><img width="80" height="80" src="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog1-80x80.jpg" class="scale-with-grid wp-post-image" alt="" srcset="https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog1-80x80.jpg 80w, https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog1-150x150.jpg 150w, https://demos.mictronicx.com/shajar-e-hayat/wp-content/uploads/2014/05/home_eco_blog1-85x85.jpg 85w" sizes="(max-width: 80px) 100vw, 80px" /><span class="c">0</span></div><div class="desc"><h6>Vestibulum commodo volutpat laoreet</h6><span class="date"><i class="icon-clock"></i>May 8, 2014</span></div></a></li></ul></div>
</aside></div><div class="column one-fourth"><aside id="text-3" class="widget widget_text"><h4>Programs</h4>            <div class="textwidget"><p style="margin-right: 5%;">Vitae adipiscing turpis. Aenean ligula nibh in, molestie id viverra a, dapibus at dolor elit lectus felis.</p>
<ul style="line-height: 30px;">
<li><i class="icon-right-circled" style="color: #ffffff;"></i> <a href="#">Mauris in erat justo venenatis </a></li>
<li><i class="icon-right-circled" style="color: #ffffff;"></i> <a href="#">Interdum nulla lectus vel</a></li>
<li><i class="icon-right-circled" style="color: #ffffff;"></i> <a href="#">Ut enim ad minim veniam</a></li>
</ul></div>
        </aside></div><div class="column one-fourth"><aside id="text-4" class="widget widget_text"><h4>The company</h4>         <div class="textwidget"><h6>Nulla risus ante, luctus et placerat quis, efficitur nec nisl. Cras iaculis tristique.</h6><p>Etiam at nibh turpis! Vestibulum mattis risus eget dolor finibus, ut luctus est congue. Ut sit amet interdum erat; quis malesuada lacus. Sed mauris diam, sodales a imperdiet ut.</p></div>
        </aside></div></div></div>
        
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">

                        <a id="back_to_top" class="button button_js" href=""><i class="icon-up-open-big"></i></a>
                        <!-- Copyrights -->
                        <div class="copyright">
                            asdasdasdasdasdas                       </div>

                        <ul class="social"></ul>
                    </div>
                </div>
            </div>

        
        
    </footer>

</div><!-- #Wrapper -->




<!-- mfn_hook_bottom --><!-- mfn_hook_bottom -->
<!-- wp_footer() -->
            <script type="text/javascript">
                function revslider_showDoubleJqueryError(sliderID) {
                    var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                    errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                    errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                    errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                    errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                        jQuery(sliderID).show().html(errorMessage);
                }
            </script>
            <script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/demos.mictronicx.com\/shajar-e-hayat\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"},"recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"cached":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.0.1'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/widget.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/mouse.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/sortable.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/tabs.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/jquery/ui/accordion.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/js/plugins.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/js/menu.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/assets/animations/animations.min.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/assets/jplayer/jplayer.min.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/js/parallax/translate3d.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/themes/betheme/js/scripts.js?ver=20.8.8.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-includes/js/wp-embed.min.js?ver=4.9.7'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/easy-fancybox/fancybox/jquery.fancybox-1.3.8.min.js?ver=1.6.2'></script>
<script type='text/javascript' src='https://demos.mictronicx.com/shajar-e-hayat/wp-content/plugins/easy-fancybox/js/jquery.easing.min.js?ver=1.4.0'></script>
<script type="text/javascript">
jQuery(document).on('ready post-load', function(){ jQuery('.nofancybox,a.pin-it-button,a[href*="pinterest.com/pin/create"]').addClass('nolightbox'); });
jQuery(document).on('ready post-load',easy_fancybox_handler);
jQuery(document).on('ready',easy_fancybox_auto);</script>

</body>
</html>
